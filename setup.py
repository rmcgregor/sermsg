from setuptools import setup, find_packages

setup(
    packages=find_packages(exclude=["*test"]),
    setup_requires=['pytest-runner', 'setuptools_scm'],
    install_requires=[
        'docopt>=0.6.2',
        'enum34>=1.1.6;python_version<"3.4"',
        'future>=0.16.0',
        'Jinja2>=2.9.6'
    ],
    tests_require=[
        'pycparser',
        'pytest>3,!=3.2.0'
    ],
    use_scm_version={"root": ".", "relative_to": __file__},
    entry_points={"console_scripts": ["sermsg-codegen = sermsg.codegen.codegen:main"]}
)
