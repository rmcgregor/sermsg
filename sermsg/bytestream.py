from __future__ import absolute_import, print_function, unicode_literals, division

import struct

from .error import MoreBytesError


def sign_extend(value, width):
    if value & (1 << (width - 1)):
        return value - (1 << width)
    return value


def bit_mask(width):
    return (1 << width) - 1


class ByteStream(object):
    """Object interface around a bytearray to allow easy reading/writing of values from the stream

    Supports bitwise access.
    Padding is automatically inserted any time not bitwise access is made.
    """

    def __init__(self, data=b"", read_offset=0, read_bit_offset=0, write_bit_offset=0, packing="packed"):
        self.data = bytearray(data)
        self.read_offset = read_offset
        self.read_bit_offset = read_bit_offset
        self.write_bit_offset = write_bit_offset
        self.write_offset = len(self.data)
        self.packing = packing

    def __repr__(self):
        return "ByteStream({!r})".format(self.to_bytes())

    def _read_align(self):
        self.read_bit_offset = 0

    def _write_align(self):
        self.write_bit_offset = 0

    def read_struct(self, fmt):
        """Byte aligned read access of a value defined by a struct fmt"""
        self._read_align()
        fmtsize = struct.calcsize(fmt)

        padding = 0
        if self.packing == "natural":
            padding = (fmtsize - (self.read_offset % fmtsize)) % fmtsize

        if len(self.data) - (self.read_offset + padding) < fmtsize:
            more_bytes = padding + fmtsize - (len(self.data) - self.read_offset)
            raise MoreBytesError("Need {} more bytes to unpack fmt {!r}".format(more_bytes, fmt), more_bytes)

        val = struct.unpack_from(fmt, self.data, offset=self.read_offset + padding)
        self.read_offset += fmtsize + padding
        return val

    def write_struct(self, fmt, *vals):
        """Byte aligned write access of a value defined by a struct fmt"""
        self._write_align()

        fmtsize = struct.calcsize(fmt)

        padding = 0
        if self.packing == "natural":
            padding = (fmtsize - (len(self.data) % fmtsize)) % fmtsize

        self.data.extend(bytearray(padding) + struct.pack(fmt, *vals))

    def read_bytes(self, length):
        self._read_align()

        if length > len(self):
            more_bytes = length = self.unread_len
            raise MoreBytesError("Need {} bytes bytes to read {}".format(more_bytes, length), more_bytes)

        val = self.data[self.read_offset: self.read_offset + length]
        self.read_offset += length
        return val

    def write_bytes(self, data):
        self._write_align()
        self.data.extend(data)
        self.write_offset = len(self.data)

    def find(self, *args, **kwargs):
        return self.to_bytes().find(*args, **kwargs)

    def write_bits(self, value, width):
        remaining = width

        while remaining:
            if self.write_offset >= len(self.data):
                self.data.extend(bytearray(1))

            remaining_bits = 8 - self.write_bit_offset
            write_len = remaining if remaining < remaining_bits else remaining_bits
            self.data[self.write_offset] |= (((value >> (width - remaining))
                                             & bit_mask(write_len)) << self.write_bit_offset)
            self.write_bit_offset += write_len

            self.write_offset += self.write_bit_offset // 8
            self.write_bit_offset %= 8
            remaining -= write_len

    def read_bits(self, width, signed=False):
        remaining = width
        value = 0
        read_bit_offset = (8 + self.read_bit_offset) % 8
        read_offset = self.read_offset if read_bit_offset == 0 else self.read_offset - 1

        while remaining:
            remaining_bits = 8 - read_bit_offset
            read_len = remaining if remaining < remaining_bits else remaining_bits
            value |= ((self.data[read_offset] >> read_bit_offset) & bit_mask(read_len)) << (width - remaining)
            read_bit_offset += read_len

            read_offset += read_bit_offset // 8
            read_bit_offset %= 8
            remaining -= read_len

        if signed:
            value = sign_extend(value, width)

        self.read_bit_offset = 0 if read_bit_offset == 0 else (read_bit_offset - 8)

        self.read_offset = read_offset if read_bit_offset == 0 else read_offset + 1

        return value

    def __iadd__(self, other):
        if isinstance(other, (bytes, bytearray)):
            self.write_bytes(other)
            return self

        if isinstance(other, ByteStream):
            self._write_align()
            self.data.extend(other.to_bytes())
            self.write_offset = len(self.data)
            return self

        return NotImplemented

    @property
    def remaining_len(self):
        return len(self.data) - self.read_offset

    def __add__(self, other):
        if isinstance(other, ByteStream):
            return ByteStream(self.to_bytes() + other.to_bytes())

        if isinstance(other, (bytes, bytearray)):
            return ByteStream(self.to_bytes() + other)

        return NotImplemented

    def __radd__(self, lother):
        if isinstance(lother, ByteStream):
            return ByteStream(lother + bytes(lother))

        if isinstance(lother, (bytes, bytearray)):
            return lother + self.to_bytes()

        return NotImplemented

    def __len__(self):
        return len(self.data) - self.read_offset

    def __eq__(self, other):
        if isinstance(other, (bytes, bytearray)):
            return self.to_bytes() == other

        if isinstance(other, ByteStream):
            return self.to_bytes() == bytes(other)

        return NotImplemented

    def to_bytes(self):
        return bytes(self.to_bytearray())

    def to_bytearray(self):
        return self.data[self.read_offset:]

    def __bytes__(self):
        return self.to_bytes()
