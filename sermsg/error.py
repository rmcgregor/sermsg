class Error(Exception):
    """Base class for all sermsg Field Exceptions"""


class EncodeError(Error):
    """Exception raised in case of an error during encoding"""

    def __init__(self, msg, cause):
        self.cause = cause
        super(EncodeError, self).__init__(msg, cause)

    def __repr__(self):
        return "{}({!r}, cause={!r})".format(self.__class__.__name__, self.args[0], self.cause)

    def __str__(self):
        return "{}, cause={!r}".format(self.args[0], self.cause)


class MoreBytesError(Error):
    """Raised if more bytes are needed to decode a field"""

    def __init__(self, msg, more_bytes):
        self.more_bytes = more_bytes
        super(MoreBytesError, self).__init__(msg, more_bytes)

    def __repr__(self):
        return "{}({!r}, more_bytes={})".format(self.__class__.__name__, self.args[0], self.more_bytes)

    def __str__(self):
        return "{}, more_bytes={}".format(self.args[0], self.more_bytes)


class ConstError(Error):
    """Raised when a decoded constant value field has not match the expected value"""
