from .msg import Msg
from . import field
from . import codegen
from .error import EncodeError

from pkg_resources import get_distribution, DistributionNotFound
try:
    __version__ = get_distribution(__name__).version
except DistributionNotFound:
    # package is not installed
    pass

__all__ = ["Msg", "field", "EncodeError", "codegen"]
