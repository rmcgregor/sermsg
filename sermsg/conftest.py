"""test config for doctests"""

from sermsg import Msg, field
import pytest
import enum


@pytest.fixture(autouse=True)
def add_doctest_namespace(doctest_namespace):
    """Add the doctest namespace used by the field & msg modules"""
    doctest_namespace['Msg'] = Msg
    doctest_namespace['field'] = field
    doctest_namespace['enum'] = enum
