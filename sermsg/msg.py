import sys
from . import field
from future.utils import with_metaclass, raise_with_traceback
from collections import OrderedDict

from .error import EncodeError
from .bytestream import ByteStream


class MsgMeta(type):
    """Metaclass to intercept all class attributes which are instances of Field"""
    def __new__(cls, name, bases, dct):
        fields = []
        for attr_name, attr in dct.items():
            if isinstance(attr, field.Field):
                fields.append((attr_name, attr))

        dct['_field_names'] = []
        if len(fields):
            fields = sorted(fields, key=lambda x: x[1].creation_idx)
            dct['_field_names'] = list(zip(*fields))[0]

        return super(MsgMeta, cls).__new__(cls, name, bases, dct)

    def __init__(cls, name, bases, dct):
        # __set_name__ was added in py3.6
        if sys.version_info < (3, 6):
            for attr_name, attr in dct.items():
                if isinstance(attr, field.Field):
                    attr.__set_name__(cls, attr_name)

        # byte order
        # inherit from module if not defined on the class
        if "_byte_order" not in dct:
            cls._byte_order = getattr(sys.modules[cls.__module__], 'sermsg_byte_order', None)

        if cls._byte_order not in ("<", ">", None):
            raise ValueError("byte_order must be one of {{'<', '>'}} (not {!r})".format(cls._byte_order))

        # packing
        if "_packing" not in dct:
            cls._packing = getattr(sys.modules[cls.__module__], "sermsg_packing", "packed")

        if cls._packing not in ("packed", "natural"):
            raise ValueError("packing must be one of {{'packed', 'natural'}} (not {!r})".format(cls._packing))

        super(MsgMeta, cls).__init__(name, bases, dct)

    def __iter__(cls):
        """Iterate through all fields in declaration order"""
        for name in cls._field_names:
            yield getattr(cls, name)

    @property
    def _implicit_field_names(cls):
        return [fld for fld in cls._field_names if isinstance(cls[fld], field.ImplicitField)]

    @property
    def _child_msgs(cls):
        child_msgs = []
        for fld in cls._field_names:
            child_msgs += cls[fld]._child_msgs
        return child_msgs

    def __getitem__(cls, key):
        """Field by name"""
        if key in cls._field_names:
            return getattr(cls, key)
        raise KeyError("{!r} is not a field name".format(key))


class Msg(with_metaclass(MsgMeta, object)):
    """Base class for all serializable structures

    Example:
        >>> class Foo(Msg):
        ...     a = field.Array(field.Uint8(), len_codec=field.Uint8())
        ...     b = field.NullString()
        >>> foo = Foo([1, 2, 3], "hello")
        >>> encoded = foo.encode()
        >>> decoded, remaining = Foo.decode(encoded)
        >>> assert foo == decoded
        >>> assert remaining == b""
    """
    def __init__(self, *args, **kwargs):

        pos_arg_fields = [name for name in self._field_names if not name.startswith("_")]

        if len(args) > len(pos_arg_fields):
            raise TypeError("{} positional arguments given when __init__ expects at most {}".format(
                len(args) + 1, len(pos_arg_fields) + 1))

        # fields with names starting with _ are omitted from positional args
        flds = {n: v for n, v in zip(pos_arg_fields, args)}

        unknown = set(kwargs.keys()) - set(self._field_names)
        if len(unknown):
            raise TypeError("unknown keyword argument '{}'".format(unknown.pop()))

        duplicate = set(kwargs.keys()) & set(flds.keys())
        if len(duplicate):
            raise TypeError("'{}' given as both and position and keyword argument".format(
                duplicate.pop()))

        flds.update(kwargs)

        # fill in defaults
        missing = set(self._field_names) - set(flds.keys())

        for m in missing:
            fld = getattr(self.__class__, m)
            if fld.default is not None:
                flds[m] = fld.default

        missing = set(self._field_names) - set(flds.keys()) - set(self.__class__._implicit_field_names)

        if len(missing) > 0:
            raise TypeError("__init__ requires a value for '{}'".format(missing.pop()))

        for name, val in flds.items():
            setattr(self, name, flds[name])

    def __getitem__(self, key):
        """Field value by name"""
        if key in self._field_names:
            return getattr(self, key)
        raise KeyError("{!r} is not a field name".format(key))

    def as_dict(self):
        """Convert the msg values into an OrderedDict"""
        return OrderedDict([(fldname, getattr(self, fldname)) for fldname in self._field_names])

    def encoded_len(self):
        """Encoded length in bytes"""
        return len(self.encode())

    def encode(self):
        """Encode into bytes"""
        stream = ByteStream()
        stream = self.encode_bytestream(stream)
        return stream.to_bytes()

    def encode_bytestream(self, stream, parent=None):
        try:
            for fld in type(self):
                stream = fld.encode_bytestream(stream, getattr(self, fld.name), self)
        except Exception as e:
            raise_with_traceback(EncodeError("Error encoding field {!r}".format(fld), e))
        return stream

    @classmethod
    def decode(cls, stream):
        """decode from bytes"""
        stream = ByteStream(stream)
        inst, stream = cls.decode_bytestream(stream)
        return inst, stream.to_bytes()

    @classmethod
    def decode_bytestream(cls, stream, parentcls=None, parentdct=None):
        dct = {}
        for fld in cls:
            dct[fld.name], stream = fld.decode_bytestream(stream, cls, dct)
        return cls(**dct), stream

    def __eq__(self, other):
        if type(self) != type(other):
            return NotImplemented
        try:
            for name in self.__class__._field_names:
                if getattr(self, name) != getattr(other, name):
                    return False
        except AttributeError:
            return False
        return True

    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        s = "{}(".format(self.__class__.__name__)
        s += ", ".join("{}={!r}".format(name, getattr(self, name))
                       for name in self._field_names if not name.startswith("_"))
        s += ")"
        return s
