from __future__ import print_function, absolute_import, division

import struct
import itertools
import inspect
import sys
from collections import OrderedDict

from .error import ConstError, MoreBytesError
from .bytestream import ByteStream


def _get_func_args(func):
    """returns func args and  dictionary of arg_name:default_values for the input function"""
    if sys.version_info[0] < 3:
        args, _, _, defaults = inspect.getargspec(func)
    else:
        fullspec = inspect.getfullargspec(func)
        args = fullspec.args
        defaults = fullspec.defaults

    if defaults is None:
        defaults = []

    if "self" in args:
        args = args[1:]
    return args, OrderedDict(zip(args[-len(defaults):], defaults))


class Field(object):
    """Abstract base for all fields

    A field is a codec for a msg value.
    """
    _creation_counter = itertools.count()

    def __init__(self, default=None, doc=None):
        self.name = None
        self.owner = None
        if default is not None:
            default = self.validate(default)
        self.default = default
        self.doc = doc
        self.creation_idx = next(self._creation_counter)

    def encode_bytestream(self, stream, value, msg):
        """Encode the value onto a bytestream"""
        raise NotImplementedError

    def decode_bytestream(self, stream, msgcls, msgdct):
        """Decode value from a bytestream"""
        raise NotImplementedError

    def validate(self, value):
        """Check the field value is valid"""
        return value

    def encoded_len(self, value, msg=None):
        return len(self.encode_bytestream(ByteStream(), value, msg))

    def __set_name__(self, msgcls, name):
        """Called after the parent class has been created

        Useful place to verify Msg format.
        """
        self.owner = msgcls
        self.name = name

    def __get__(self, msg, msgcls=None):
        if msg is None:
            return self
        return msg.__dict__[self.name]

    def __set__(self, msg, value):
        msg.__dict__[self.name] = self.validate(value)

    def _repr_args(self):
        """get a list of a the args strings to place in the repr"""
        args, defaults = _get_func_args(self.__init__)
        repr_args = []
        for a in args:
            try:
                if a not in defaults:
                    repr_args.append(repr(getattr(self, a)))
                else:
                    val = getattr(self, a)
                    if val != defaults[a]:
                        repr_args.append("{}={!r}".format(a, val))
            except AttributeError:
                pass
        return repr_args

    def __repr__(self):
        args = self._repr_args()
        args = ", ".join(args)
        s = "{}({})".format(self.__class__.__name__, args)

        if self.name is not None and self.owner is not None:
            s = "<{}.{}: {}>".format(self.owner.__name__, self.name, s)
        return s

    @property
    def _child_msgs(self):
        """List of Msg cls references held by this field

        This used to resolve the dependency order of msgs
        """
        return []


class ImplicitField(Field):
    """Abstract Base for fields whose value is not explicitly set"""
    def __init__(self, codec, **kwargs):
        self.codec = codec
        super(ImplicitField, self).__init__(**kwargs)

    def __set__(self, msg, value):
        pass

    def encode_bytestream(self, stream, value, msg):
        return self.codec.encode_bytestream(stream, value, msg)

    def decode_bytestream(self, stream, msgcls, msgdct):
        return self.codec.decode_bytestream(stream, msgcls, msgdct)


class PrimitiveField(Field):
    """Abstract base for fields encoded with struct module primitives"""
    _default_byte_order = "<"

    def __init__(self, byte_order=None, **kwargs):
        if byte_order not in ("<", ">", None):
            raise ValueError("byte_order must be one of {{'<', '>'}} (not {!r})".format(byte_order))

        self._byte_order = byte_order
        super(PrimitiveField, self).__init__(**kwargs)

    @property
    def byte_order(self):
        if self._byte_order is not None:
            bo = self._byte_order
        elif self.owner is not None and self.owner._byte_order is not None:
            bo = self.owner._byte_order
        else:
            bo = self._default_byte_order
        return bo

    @property
    def fmt(self):
        fmt = self.byte_order
        return fmt + self._fmt

    def encoded_len(self, value=None, msg=None):
        return struct.calcsize(self.fmt)

    def encode_bytestream(self, stream, value, msg):
        stream.write_struct(self.fmt, value)
        return stream

    def decode_bytestream(self, stream, msgcls, msgdct):
        val = stream.read_struct(self.fmt)[0]
        return val, stream


class PrimitiveFloatingPointField(PrimitiveField):
    """Abstract base for floating points fields"""

    def __init__(self, min=None, max=None, **kwargs):
        super(PrimitiveFloatingPointField, self).__init__(**kwargs)
        self.min = min
        self.max = max

    def validate(self, value):
        value = float(value)
        if self.max is not None and value > self.max:
            raise ValueError("{!r} is greater than the {} maximum".format(value, self.max))
        if self.min is not None and value < self.min:
            raise ValueError("{!r} is less than the {} minimum".format(value, self.max))
        return value


class Float32(PrimitiveFloatingPointField):
    _fmt = "f"


class Float64(PrimitiveFloatingPointField):
    _fmt = "d"


class PrimitiveIntegerField(PrimitiveField):
    """Abstract base for all integers"""

    def __init__(self, min=None, max=None, **kwargs):
        self._min = min
        self._max = max
        super(PrimitiveIntegerField, self).__init__(**kwargs)

    @property
    def range(self):
        fmt_bits = {"B": 8, "H": 16, "I": 32, "Q": 64}

        bits = fmt_bits[self._fmt.upper()]
        signed = self._fmt.islower()

        if signed:
            return (-2**(bits - 1), 2**(bits - 1) - 1)
        else:
            return (0, 2**bits - 1)

    @property
    def max(self):
        if self._max is not None:
            return self._max
        return self.range[1]

    @property
    def min(self):
        if self._min is not None:
            return self._min
        return self.range[0]

    def validate(self, value):
        value = int(value)
        if value > self.max:
            raise ValueError("{!r} is greater than the {} maximum".format(value, self.max))
        if value < self.min:
            raise ValueError("{!r} is less than the {} minimum".format(value, self.max))
        return value

    def encode_bytestream(self, stream, value, msg):
        value = self.validate(value)
        return super(PrimitiveIntegerField, self).encode_bytestream(stream, value, msg)


class Uint8(PrimitiveIntegerField):
    _fmt = "B"


class Int8(PrimitiveIntegerField):
    _fmt = "b"


class Uint16(PrimitiveIntegerField):
    _fmt = "H"


class Int16(PrimitiveIntegerField):
    _fmt = "h"


class Uint32(PrimitiveIntegerField):
    _fmt = "I"


class Int32(PrimitiveIntegerField):
    _fmt = "i"


class Uint64(PrimitiveIntegerField):
    _fmt = "Q"


class Int64(PrimitiveIntegerField):
    _fmt = "q"


class Bool(Field):
    _fmt = "B"

    def __init__(self, codec=Uint8(), **kwargs):
        super(Bool, self).__init__(**kwargs)
        self.codec = codec

    def encode_bytestream(self, stream, value, msg):
        return self.codec.encode_bytestream(stream, value, msg)

    def decode_bytestream(self, stream, msgcls, msgdct):
        return self.codec.decode_bytestream(stream, msgcls, msgdct)

    def validate(self, value):
        return bool(value)


class StringField(Field):
    """Abstract base for all String type fields"""

    def __init__(self, encoding='ascii', **kwargs):
        self.encoding = encoding
        super(StringField, self).__init__(**kwargs)

    def validate(self, value):
        value = str(value)
        if self.max_len is not None and len(value.encode()) > self.max_len:
            raise ValueError("{!r} is longer than max byte length of {}".format(value,
                                                                                self.max_len))
        return value


class NullString(StringField):
    r"""String delimited with a NULL character (C string)

    Example:
        >>> class Foo(Msg):
        ...     c_str = field.NullString()
        >>> assert Foo("hello").encode() == b"hello\x00"
    """

    def __init__(self, max_len=None, **kwargs):
        self.max_len = int(max_len) if max_len is not None else None
        super(NullString, self).__init__(**kwargs)

    def encode_bytestream(self, stream, value, msg):
        encoded = value.encode(self.encoding)
        if encoded[-1] != b"\x00":
            encoded += b"\x00"
        return stream + encoded

    def decode_bytestream(self, stream, msgcls, msgdct):
        idx = stream.find(b"\x00")
        if idx == -1:
            raise ValueError("Could not find NULL character in {!r}".format(stream))

        value = stream.read_bytes(idx + 1)[:-1].decode(self.encoding)
        return value, stream


class String(StringField):
    """String prepended with its byte length"""

    def __init__(self, len_codec=Uint16(), **kwargs):
        super(String, self).__init__(**kwargs)
        if not isinstance(len_codec, PrimitiveIntegerField):
            raise TypeError(
                "len_codec must be an instance of PrimitiveIntegerField (not {})".format(len_codec))
        self.len_codec = len_codec

    @property
    def max_len(self):
        return self.len_codec.max

    def encode_bytestream(self, stream, value, msg):
        encoded = value.encode(self.encoding)
        stream = self.len_codec.encode_bytestream(stream, len(encoded), msg)
        return stream + encoded

    def decode_bytestream(self, stream, msgcls, msgdct):
        length, stream = self.len_codec.decode_bytestream(stream, msgcls, msgdct)
        value = stream.read_bytes(length).decode(self.encoding)
        return value, stream


class FixedLenString(StringField):
    """String with a fixed byte length"""

    def __init__(self, length, **kwargs):
        super(FixedLenString, self).__init__(**kwargs)
        self.length = int(length)

    @property
    def max_len(self):
        return self.length

    def encode_bytestream(self, stream, value, msg):
        encoded = value.encode(self.encoding)
        if len(encoded) < self.length:
            encoded += b"\x00" * (self.length - len(encoded))
        return stream + encoded

    def encoded_len(self, value=None, msg=None):
        return self.length

    def decode_bytestream(self, stream, msgcls, msgdct):
        value = stream.read_bytes(self.length).decode(self.encoding).rstrip("\x00")
        return value, stream


class UnboundedString(StringField):
    """String what only ends when the bytestream does"""

    def __init__(self, max_len=None, **kwargs):
        super(UnboundedString, self).__init__(**kwargs)
        self.max_len = int(max_len) if max_len is not None else None

    def encode_bytestream(self, stream, value, msg):
        encoded = value.encode(self.encoding)
        if self.max_len is not None and len(encoded) > self.max_len:
            raise ValueError("{!r} length is {} greater than the max".format(self, len(encoded)))
        stream += encoded
        return stream

    def decode_bytestream(self, stream, msgcls, msgdct):
        if self.max_len is not None and len(stream) > self.max_len:
            raise ValueError("{!r} length of stream {} is greater than the max".format(self,
                                                                                       len(stream)))
        return stream.read_bytes(len(stream)).decode(self.encoding), stream


class BytesField(Field):
    """Abstract base for all Bytes based fields"""

    @property
    def max_len(self):
        return None

    def validate(self, value):
        return bytes(value)


class Bytes(BytesField):
    """Raw data prepended with its length"""

    def __init__(self, len_codec=Uint16(), **kwargs):
        if not isinstance(len_codec, Field):
            raise TypeError("len_codec must be an instance of Field (not {})".format(len_codec))
        self.len_codec = len_codec

        super(Bytes, self).__init__(**kwargs)

    @property
    def max_len(self):
        return self.len_codec.max

    def encode_bytestream(self, stream, value, msg):
        return self.len_codec.encode_bytestream(stream, len(value), msg) + value

    def decode_bytestream(self, stream, msgcls, msgdct):
        length, stream = self.len_codec.decode_bytestream(stream, msgcls, msgdct)
        value = stream.read_bytes(length)
        return value, stream


class FixedLenBytes(BytesField):
    """A fixed length of bytes"""

    def __init__(self, length, **kwargs):
        self.length = length
        super(FixedLenBytes, self).__init__(**kwargs)

    def validate(self, value):
        value = super(FixedLenBytes, self).validate(value)
        if len(value) != self.length:
            raise ValueError("value must be a bytes object of length {} (not {!r} len={!r})".format(
                self.length, value, len(value)))
        return value

    @property
    def max_len(self):
        return self.length

    def encode_bytestream(self, stream, value, msg):
        if len(value) != self.length:
            raise ValueError("length of {!r} must be {} not {}".format(self, self.length, len(
                value)))
        return stream + value

    def decode_bytestream(self, stream, msgcls, msgdct):
        return stream.read_bytes(self.length), stream


class LengthedBytes(BytesField):
    """Bytes with a separate explicit length field"""

    def __init__(self, len_field, **kwargs):
        super(LengthedBytes, self).__init__(**kwargs)
        self.len_field = len_field

    def encode_bytestream(self, stream, value, msg):
        if len(value) != msg[self.len_field.name]:
            raise ValueError(
                "Length of bytes {} does not match its length field {!r} with value {}".format(
                    len(value), self.len_field.name, msg[self.len_field.name]))
        return stream + value

    def decode_bytestream(self, stream, msgcls, msgdct):
        length = msgdct[self.len_field.name]
        return stream.read_bytes(length), stream


class UnboundedBytes(BytesField):
    """Bytes that consume all the available bytestream"""

    def __init__(self, max_len=None, offset=0, **kwargs):
        super(UnboundedBytes, self).__init__(**kwargs)
        self._max_len = max_len
        self.offset = offset

        if self.offset > 0:
            raise ValueError("Offset must be less than or equal to zero")

    @property
    def max_len(self):
        return self._max_len

    def encode_bytestream(self, stream, value, msg):
        if self.max_len is not None and len(value) > self.max_len:
            raise ValueError("{!r} length is {} greater than the max".format(self, len(value)))
        return stream + value

    def decode_bytestream(self, stream, msgcls, msgdct):
        value = stream.read_bytes(len(stream) + self.offset)

        if self.max_len is not None and len(value) > self.max_len:
            raise ValueError("{!r} length of stream {} is greater than the max".format(self,
                                                                                       len(value)))
        return value, stream


class ArrayField(Field):
    """Abstract Base for all Array type fields"""

    def __init__(self, entry, **kwargs):
        if not isinstance(entry, Field):
            entry = SubMsg(entry)
        self.entry = entry
        super(ArrayField, self).__init__(**kwargs)

    @property
    def max_len(self):
        return None

    def validate(self, value):
        return [self.entry.validate(e) for e in value]

    @property
    def _child_msgs(self):
        return self.entry._child_msgs


class Array(ArrayField):
    r"""Variable length Array.

    Encoded prepended by its length

    Example:
        >>> class Foo(Msg):
        ...    a = field.Array(field.Int8(), len_codec=field.Uint16())
        >>> assert Foo([0x12, 0x13, 0x14, 0x15]).encode() == b'\x04\x00\x12\x13\x14\x15'
    """

    def __init__(self, entry, len_codec=Uint16(), **kwargs):
        self.len_codec = len_codec
        super(Array, self).__init__(entry, **kwargs)

    def encode_bytestream(self, stream, value, msg):
        stream = self.len_codec.encode_bytestream(stream, len(value), msg)
        for e in value:
            stream = self.entry.encode_bytestream(stream, e, msg)
        return stream

    @property
    def max_len(self):
        return self.len_codec.max

    def decode_bytestream(self, stream, msgcls, msgdct):
        length, stream = self.len_codec.decode_bytestream(stream, msgcls, msgdct)
        values = []
        for _ in range(length):
            val, stream = self.entry.decode_bytestream(stream, msgcls, msgdct)
            values.append(val)

        return values, stream


class FixedLenArray(ArrayField):
    """Array of fixed length

    Example:
        >>> class Foo(Msg):
        ...     a = field.FixedLenArray(field.Uint16(), 5)
        >>> foo = Foo([1, 2, 3, 4, 5])
    """

    def __init__(self, entry, length, **kwargs):
        self.length = length
        super(FixedLenArray, self).__init__(entry, **kwargs)

    def encode_bytestream(self, stream, value, msg):
        for e in value:
            stream = self.entry.encode_bytestream(stream, e, msg)
        return stream

    @property
    def max_len(self):
        return self.length

    def encoded_len(self, value=None, msg=None):
        if value is not None:
            return sum(self.entry.encoded_len(v, msg) for v in value)
        return self.entry.encoded_len() * self.length

    def decode_bytestream(self, stream, msgcls, msgdct):
        vals = []
        for _ in range(self.length):
            val, stream = self.entry.decode_bytestream(stream, msgcls, msgdct)
            vals.append(val)
        return vals, stream


class UnboundedArray(ArrayField):
    r"""Array that absorbs all available bytestream

    Example:
        >>> class Foo(Msg):
        ...     data = field.UnboundedArray(field.Uint8())
        >>> Foo.decode(b"\x01\x02\x03")[0]
        Foo(data=[1, 2, 3])
    """

    def __init__(self, entry, max_len=None, **kwargs):
        self._max_len = max_len
        super(UnboundedArray, self).__init__(entry, **kwargs)

    @property
    def max_len(self):
        return self._max_len

    def encode_bytestream(self, stream, value, msg):
        if self.max_len is not None and len(value) > self.max_len:
            raise ValueError("{!r} length is {} greater than the max".format(self, len(value)))

        for e in value:
            stream = self.entry.encode_bytestream(stream, e, msg)
        return stream

    def decode_bytestream(self, stream, msgcls, msgdct):
        vals = []
        try:
            while True:
                val, stream = self.entry.decode_bytestream(stream, msgcls, msgdct)
                vals.append(val)
                if self.max_len is not None and len(vals) > self.max_len:
                    raise ValueError("{!r} length of array {} is greater than the max".format(
                        self, len(stream)))

        except MoreBytesError:
            pass

        return vals, stream


class DelimitedArray(ArrayField):
    r"""Array whose length is capped by a delimiter

    Example:
        >>> class Entry(Msg):
        ...     a = field.Uint8()
        >>> null_entry = Entry(0)
        >>> class Foo(Msg):
        ...     array = field.DelimitedArray(field.SubMsg(Entry), null_entry)
        >>> Foo.decode(b"\x02\x03\x04\x05\x00")[0]
        Foo(array=[Entry(a=2), Entry(a=3), Entry(a=4), Entry(a=5)])
    """

    def __init__(self, entry, delimiter, max_len=None, **kwargs):
        super(DelimitedArray, self).__init__(entry, **kwargs)
        self.delimiter = delimiter
        self._max_len = max_len

    @property
    def man_len(self):
        return self._max_len

    def encode(self, value, msg=None, stream=b''):
        for e in value:
            stream = self.entry.encode(e, msg, stream)
        stream = self.entry.encode(self.delimiter, msg, stream)
        return stream

    def decode_bytestream(self, stream, msgcls, msgdct):
        vals = []
        while True:
            val, stream = self.entry.decode_bytestream(stream, msgcls, msgdct)
            if val == self.delimiter:
                break
            vals.append(val)
        return vals, stream


class Enum(Field):
    r"""Field encoding enum values

    Example:
        >>> class Colour(enum.Enum):
        ...     RED = 1
        ...     BLUE = 2
        >>> class Foo(Msg):
        ...     colour = field.Enum(Colour)
        >>> assert Foo(Colour.RED).encode() == b"\x01"
    """

    def __init__(self, enum, codec=Uint8(), **kwargs):
        self.enum = enum
        self.codec = codec
        super(Enum, self).__init__(**kwargs)

    def validate(self, value):
        if value not in self.enum:
            raise TypeError("{} is not a member of {}".format(value, self.enum))
        return value

    def encode_bytestream(self, stream, value, msg):
        return self.codec.encode_bytestream(stream, value.value, msg)

    def decode_bytestream(self, stream, msgcls, msgdct):
        val, stream = self.codec.decode_bytestream(stream, msgcls, msgdct)
        val = self.enum(val)
        return val, stream


class SubMsg(Field):
    """Field for nested Msg

    Example:
        >>> class Bar(Msg):
        ...     a = field.Uint16()
        >>> class Foo(object):
        ...     nested = field.SubMsg(Bar)
    """

    def __init__(self, msgcls, **kwargs):
        self.msgcls = msgcls
        super(SubMsg, self).__init__(**kwargs)

    def validate(self, value):
        if not isinstance(value, self.msgcls):
            raise TypeError("{!r} is not of type {!r}".format(value, self.msgcls))
        return value

    def encode_bytestream(self, stream, value, msg):
        return value.encode_bytestream(stream, parent=msg)

    def decode_bytestream(self, stream, msgcls, msgdct):
        return self.msgcls.decode_bytestream(stream, parentcls=msgcls, parentdct=msgdct)

    @property
    def _child_msgs(self):
        return [self.msgcls]


class SwitchField(Field):
    """Abstract base for switch fields"""
    def __init__(self, options, **kwargs):
        super(SwitchField, self).__init__(**kwargs)
        if isinstance(options, (list, tuple)):
            options = {i: v for i, v in enumerate(options)}
        self.options = options

    def __getitem__(self, key):
        """Lookup the option from the key"""
        return self.options[key]

    def lookup_key(self, value):
        """Lookup the key from the option"""
        matches = [k for k, v in self.options.items() if v is value.__class__]
        if len(matches) == 0:
            raise LookupError("No key matching found for value={}".format(value))
        elif len(matches) > 1:
            raise LookupError("value={} ambiguously matches keys={}".format(value, matches))
        return matches[0]

    def __set__(self, msg, value):
        if value.__class__ not in self.options.values():
            raise TypeError("{!r} is a not a valid option".format(value))
        super(SwitchField, self).__set__(msg, value)

    def _encode_value(self, stream, value, parent):
        return value.encode_bytestream(stream, parent)

    def _decode_value(self, valuecls, stream, parentcls, parentdct):
        return valuecls.decode_bytestream(stream, parentcls, parentdct)

    @property
    def _child_msgs(self):
        return list(self.options.values())

    def items(self):
        for k, v in self.options.items():
            yield k, v


class Switch(SwitchField):
    r"""Field with variable payload codec

    A key followed by the encoded value.
    Both the key and value must be unique.

    Example:
        >>> class Bar1(Msg):
        ...     a = Uint8()
        >>> class Bar2(Msg):
        ...     b = Uint16()
        >>> class Foo(Msg):
        ...     body = field.Switch({1: Bar1, 2: Bar2})
        >>> assert Foo(Bar1(0x12)).encode() == b"\x01\x12"
    """

    def __init__(self, options, key_codec=Uint8(), **kwargs):
        super(Switch, self).__init__(options, **kwargs)
        self.key_codec = key_codec

    def encode_bytestream(self, stream, value, msg):
        key = self.lookup_key(value)
        stream = self.key_codec.encode_bytestream(stream, key, msg)
        stream = self._encode_value(stream, value, msg)
        return stream

    def decode_bytestream(self, stream, msgcls, msgdct):
        key, stream = self.key_codec.decode_bytestream(stream, msgcls, msgdct)
        option = self[key]
        return self._decode_value(option, stream, msgcls, msgdct)


class KeyedSwitch(SwitchField):
    """Switch with separate explicit key field.

    The key must appear before the value in the Msg field ordering,
    but the two can be seperated with other fields.

    The Key field can be used to implicitly calculate the key from the value
    when there is a 1 to 1 key <-> value mapping.
    Overwise the key value will have to be explicitly passed.

    Example:
        >>> class Bar1(Msg):
        ...     a = Uint8()
        >>> class Bar2(Msg):
        ...     b = Uint16()
        >>> class ImplicitFoo(Msg):
        ...     _key = field.Key("value", codec=field.Uint8())
        ...     _padding = field.Const(field.Uint8(), 20)
        ...     value = field.KeyedSwitch(_key, {1: Bar1, 2: Bar2})
        >>> class ExplicitFoo(Msg):
        ...     key = field.Uint8()
        ...     _padding = field.Const(field.Uint8(), 0xa5)
        ...     value = field.KeyedSwitch(key, {0: Bar1, 2: Bar1, 10: Bar2})
    """
    def __init__(self, key_field, options, **kwargs):
        super(KeyedSwitch, self).__init__(options, **kwargs)
        self.key_field = key_field

    def encode_bytestream(self, stream, value, msg):
        return self._encode_value(stream, value, msg)

    def decode_bytestream(self, stream, msgcls, msgdct):
        key = msgdct[self.key_field.name]
        option = self[key]
        return self._decode_value(option, stream, msgcls, msgdct)


class Key(ImplicitField):
    """For use with KeyedSwitch Fields where the key can be implicitly resolved from the value.

    Does not support ambiguous value -> key relationships.
    These must be explicitly filled in by the user using the underlying codec field.

    Calls lookup_key(value) on the associated field.

    Example:
        >>> class Bar1(Msg):
        ...     a = field.Uint8()
        >>> class Bar2(Msg):
        ...     b = field.Uint16()
        >>> class Foo(Msg):
        ...     _key = field.Key("body")
        ...     body = field.KeyedSwitch(_key, {1: Bar1, 2: Bar2})
    """
    def __init__(self, switch_field, codec=Uint8(), **kwargs):
        super(Key, self).__init__(codec, **kwargs)
        self.switch_field = switch_field

    def __get__(self, msg, msgcls):
        if msg is None:
            return self

        switch_field = msgcls[self.switch_field]
        key = switch_field.lookup_key(msg[self.switch_field])
        return key


class Conditional(Field):
    r"""Field whose presence is dependant on some condition

    Example:
        >>> class Foo(Msg):
        ...    flag = field.Bool()
        ...    s = field.Conditional(field.NullString(), lambda x: x["flag"])
        >>> assert Foo(False, "bob").encode() == b"\x00"
        >>> assert Foo(True, "bill").encode() == b"\x01bill\x00"
    """

    def __init__(self, entry, condition, **kwargs):
        self.entry = entry
        self.condition = condition
        super(Conditional, self).__init__(**kwargs)

    def encode_bytestream(self, stream, value, msg):
        if value is not None and self.condition(msg.as_dict()):
            stream = self.entry.encode_bytestream(stream, value, msg)
        return stream

    def decode_bytestream(self, stream, msgcls, msgdct):
        val = None
        if self.condition(msgdct):
            val = self.entry.decode_bytestream(stream, msgcls, msgdct)
        return val, stream


class Const(ImplicitField):
    """Field with a constant value

    Example:
        >>> class Foo(Msg):
        ...     constant = field.Const(field.Uint8(), 0x45)
        >>> foo = Foo()
        >>> assert foo.constant == 0x45
    """
    def __init__(self, codec, value, **kwargs):
        if not isinstance(codec, Field):
            raise TypeError("codec must be an instance of Field (not {!r})".format(codec))

        self.codec = codec
        value = self.codec.validate(value)
        kwargs["default"] = value
        super(Const, self).__init__(codec, **kwargs)

    @property
    def value(self):
        return self.default

    def __get__(self, msg, msgcls):
        if msg is None:
            return self
        return self.value

    def decode_bytestream(self, stream, msgcls, msgdct):
        value, stream = super(Const, self).decode_bytestream(stream, msgcls, msgdct)
        if value != self.value:
            raise ConstError("Decoded value {!r} does not match expected value {!r}".format(
                value, self.value))


class EncodedLen(ImplicitField):
    """Encoded length over other field(s)

    Called and sums encoded_len on with associated fields

    Example:
        >>> class Foo(Msg):
        ...     a = field.Uint8()
        ...     _length = field.EncodedLen([a, "b"]) # refs or names are ok
        ...     b = field.FixedLenArray(field.Uint16(), 2)
        >>> assert Foo(a=1, b=[2, 3])._length == 5
    """
    def __init__(self, length_of, codec=Uint8(), **kwargs):
        self.length_of = length_of
        super(EncodedLen, self).__init__(codec, **kwargs)

    def __get__(self, msg, msgcls):
        if msg is None:
            return self

        if isinstance(self.length_of, list):
            length_of = self.length_of
        else:
            length_of = [self.length_of]

        length = 0
        for fld in length_of:
            # if we were given strings convert them to field refs
            length += fld.encoded_len(getattr(msg, fld.name))
        return length

    def __set_name__(self, owner, name):
        super(EncodedLen, self).__set_name__(owner, name)
        # resolve all the named fields into real refs

        if isinstance(self.length_of, (list, tuple)):
            length_of = []
            for fld in self.length_of:
                if isinstance(fld, str):
                    length_of.append(owner[fld])
                elif isinstance(fld, Field):
                    length_of.append(fld)
                else:
                    raise TypeError("length_of must contain Fields or names of Fields (not {})".format(fld))
            self.length_of = length_of

        elif isinstance(self.length_of, str):
            self.length_of = owner[self.length_of]

    @property
    def max(self):
        self.codec.max

    @property
    def min(self):
        self.codec.min


class Len(ImplicitField):
    """Length of another field value

    Calls len() on the associated fields value

    Example:
        >>> class Foo(Msg):
        ...     _len = field.Len("list") # ref or name is ok
        ...     list = field.Array(field.Uint8())
        >>> assert Foo([2, 3, 4, 5])._len == 4
    """

    def __init__(self, length_of, codec=Uint8(), **kwargs):
        if not isinstance(length_of, (str, Field)):
            raise TypeError("length_of must be a field name or reference (not {})".format(length_of))

        self.length_of = length_of
        super(Len, self).__init__(codec, **kwargs)

    def __get__(self, msg, msgcls):
        if msg is None:
            return self

        length = len(getattr(msg, self.length_of.name))
        return length

    def __set_name__(self, owner, name):
        super(Len, self).__set_name__(owner, name)
        # resolve all the named fields into real refs
        if isinstance(self.length_of, str):
            self.length_of = getattr(owner, self.length_of)

    @property
    def max(self):
        self.codec.max

    @property
    def min(self):
        self.codec.min


class Bits(Field):
    r"""A integer bit field of given width

    Example:
        >>> class Foo(Msg):
        ...     a = field.Bits(4)
        ...     b = field.Bits(4)
        ...     c = field.Bits(2)
        ...     d = field.Uint8() # automatic realigns to the next byte boundary
        >>> assert Foo(4, 5, 2, 0xFF).encode() == b"\x54\x02\xff"
    """
    def __init__(self, width, signed=False, **kwargs):
        super(Bits, self).__init__(**kwargs)
        self.width = width
        self.signed = signed

    def validate(self, value):
        value = int(value)

        if value > self.max or value < self.min:
            raise ValueError("{} is out of range {}".format(value, self.range))

        return value

    @property
    def range(self):
        if self.signed:
            half_width = (1 << (self.width - 1))
            return (-half_width, half_width - 1)
        else:
            return (0, (1 << self.width) - 1)

    @property
    def max(self):
        return self.range[1]

    @property
    def min(self):
        return self.range[0]

    def decode_bytestream(self, stream, msgcls, msgdct):
        value = stream.read_bits(self.width, self.signed)
        return value, stream

    def encode_bytestream(self, stream, value, msg):
        stream.write_bits(value, self.width)
        return stream
