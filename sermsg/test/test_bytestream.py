from sermsg.bytestream import ByteStream
from sermsg.error import MoreBytesError
import pytest

@pytest.mark.parametrize("fmt,val", [
    ("<I", 20),
    ("<h", -6),
    ("<Q", 2030)
])
def test_read_write_fmt(fmt, val):
    stream = ByteStream()
    stream.write_struct(fmt, val)
    print(stream)
    read_struct = stream.read_struct(fmt)[0]
    assert read_struct == val


def test_natural_packing():
    stream = ByteStream(packing="natural")
    stream.write_struct("<H", 20)
    stream.write_struct("<I", 567)
    assert 20 == stream.read_struct("<H")[0]
    assert 567 == stream.read_struct("<I")[0]


@pytest.mark.parametrize("value,width,signed", [
    (40, 8, False),
    (-30, 12, True),
    (100, 8, True),
    (12222, 16, False),
])
def test_bits(value, width, signed):
    stream = ByteStream()
    stream.write_bits(value, width)
    read_val = stream.read_bits(width, signed)
    assert value == read_val


@pytest.mark.parametrize("vals", [
    [(40, 8, False),
    (-30, 12, True),
    (100, 8, True),
    (12222, 16, False)]
])
def test_bits_batch(vals):
    stream = ByteStream()

    for value, width, signed in vals:
        stream.write_bits(value, width)
        read_val = stream.read_bits(width, signed)
        assert value == read_val


def test_read_realign():
    stream = ByteStream()
    stream.write_bits(0x20, 12)
    stream.write_bits(0x21, 7)
    stream.write_bits(0x22, 6)
    stream.write_struct("<H", 0x1234)

    print(stream)

    assert stream.read_bits(12) == 0x20
    assert stream.read_bits(7) == 0x21
    assert stream.read_bits(6) == 0x22

    assert stream.read_struct("<H")[0] == 0x1234


def test_more_bytes_error():
    stream = ByteStream()
    with pytest.raises(MoreBytesError):
        stream.read_struct("<H")[0]


def test_bits_on_byte_boundaries():
    stream = ByteStream()
    stream.write_bits(0x20, 8)
    stream.write_bits(0x1234, 16)

    assert stream.read_bits(8) == 0x20
    assert stream.read_bits(16) == 0x1234


def test_concat_bytes():
    stream = ByteStream(b"1234")
    stream += b"5678"
    assert stream.data == b"12345678"