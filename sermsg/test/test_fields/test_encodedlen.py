from __future__ import print_function, absolute_import

from sermsg import Msg, field
import pytest


class Bar(Msg):
    len = field.EncodedLen("b")
    b = field.NullString()


class MultiBar(Msg):
    len = field.EncodedLen(["a", "b", "c"])
    a = field.Uint32()
    b = field.NullString()
    c = field.FixedLenBytes(2)


tests = [
    (Bar(None, "hello"), 6),
    (MultiBar(None, 23, "bob", b"\x20\x20"), 10),
]


@pytest.mark.parametrize("msg,length", tests)
def test_length_of(msg, length):
    assert msg.len == length
