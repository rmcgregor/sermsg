from __future__ import print_function, absolute_import

from sermsg import Msg, field
import pytest


class Bar(Msg):
    s = field.Bool()


tests = [
    (Bar(True), b"\x01"),
    (Bar(False), b"\x00"),
]


@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remainer = msg.__class__.decode(encoded)
    assert decoded == msg
    assert len(remainer) == 0