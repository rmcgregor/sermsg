from __future__ import print_function, absolute_import

from sermsg import Msg, field, EncodeError
import pytest


class Foo1(Msg):
    a = field.Uint16()


class Foo2(Msg):
    a = field.NullString()


class ImplicitBar(Msg):
    _key = field.Key("switch", codec=field.Uint8())
    switch = field.KeyedSwitch(_key, {1: Foo1, 2: Foo2})


class ExplicitBar(Msg):
    key = field.Uint8()
    switch = field.KeyedSwitch(key, {1: Foo1, 2: Foo2})


tests = [
    (ImplicitBar(Foo1(12)), b"\x01\x0c\x00"),
    (ExplicitBar(2, Foo2("bob")), b"\x02bob\x00")
]


@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remainder = msg.__class__.decode(encoded)
    assert decoded == msg
    assert len(remainder) == 0