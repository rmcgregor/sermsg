from sermsg import Msg, field
import pytest

class Foo(Msg):
    a = field.FixedLenString(10)


class FooUTF8(Msg):
    a = field.FixedLenString(5, encoding="utf-8")


tests = [
    (Foo("hello"), b"hello"+b"\x00"*5),
    (FooUTF8(""), b"\x00"*5),
]


@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remainder = msg.__class__.decode(encoded)
    assert decoded == msg
    assert len(remainder) == 0