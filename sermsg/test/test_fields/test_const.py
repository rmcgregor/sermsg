from sermsg import Msg, field
import pytest


class Foo(Msg):
    const = field.Const(field.Uint16(), 45)
    const_array = field.Const(field.Array(field.Uint8()), [2, 3, 4, 5])


def test_get_value():
    f = Foo()
    assert f.const == Foo.const.value
    assert f.const_array == Foo.const_array.value


def test_const_validation():
    with pytest.raises(ValueError):
        class Bar(Msg):
            const = field.Const(field.Uint8(), 4000)
