from __future__ import print_function, absolute_import

from sermsg import Msg, field, EncodeError
import pytest


class ImplicitBar(Msg):
    _s_len = field.Len("s", codec=field.Uint8())
    s = field.LengthedBytes(_s_len)

class ExplicitBar(Msg):
    s_len = field.Uint8()
    s = field.LengthedBytes(s_len)


tests = [
    (ImplicitBar(b"1234"), b"\x041234"),
    (ExplicitBar(7, b"1234567"), b"\x071234567")
]

@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remainder = msg.__class__.decode(encoded)
    assert decoded == msg
    assert len(remainder) == 0


def test_len_mismatch():
    with pytest.raises(EncodeError):
        bar = ExplicitBar(3, b"qq3887")
        bar.encode()