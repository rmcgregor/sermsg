from __future__ import print_function, absolute_import

from sermsg import Msg, field
import pytest


class BarString(Msg):
    _len = field.Len("s")
    s = field.NullString()

class BarArray(Msg):
    _len = field.Len("array")
    array = field.Array(field.Uint16())


tests = [
    (BarString("hello"), 5),
    (BarArray([1, 2, 3]), 3),
]

@pytest.mark.parametrize("msg,length", tests)
def test_length_of(msg, length):
    assert msg._len == length


def test_bad_init_value():
    with pytest.raises(TypeError):
        class Foo(Msg):
            a = field.Len(["x", "y"])
            x = field.Uint16()
            y = field.Uint32()