from __future__ import print_function, absolute_import

from sermsg import Msg, field
import pytest


class Bar(Msg):
    s = field.String(len_codec=field.Uint8())


class NullBar(Msg):
    s = field.NullString()


class UnboundedBar(Msg):
    s = field.UnboundedString()

tests = [
    (Bar("hello world"), b"\x0bhello world"),
    (NullBar("hello world"), b"hello world\x00"),
    (UnboundedBar("hello world"), b"hello world")
]

@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remainder = msg.__class__.decode(encoded)
    assert decoded == msg
    assert len(remainder) == 0


def test_too_long():
    with pytest.raises(ValueError):
        Bar("A" * 300)


def test_bad_len_codec():
    with pytest.raises(TypeError):
        class Bar(Msg):
            s = field.String(len_codec=field.String())
