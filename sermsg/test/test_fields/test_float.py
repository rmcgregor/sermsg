from __future__ import print_function, absolute_import

from sermsg import Msg, field
import pytest


class Bar32(Msg):
    a = field.Float32()

class Bar64(Msg):
    a = field.Float64()


tests = [
    (Bar32(1.2), b'\x9a\x99\x99?'),
    (Bar64(45.8e-89), b"\xd3\xd39'\xb3'\xcd-"),
]


@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remainer = msg.__class__.decode(encoded)
    assert decoded.a == pytest.approx(msg.a)
    assert len(remainer) == 0