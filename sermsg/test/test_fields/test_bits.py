from sermsg import Msg, field

import pytest


class Foo(Msg):
    a = field.Bits(4)
    b = field.Bits(3)
    c = field.Bits(1)


class SignedFoo(Msg):
    a = field.Bits(4, True)
    b = field.Bits(10, True)
    c = field.Bits(3, True)


tests = [
    (Foo(6, 2, 0), b"\x26"),
    (SignedFoo(-3, -512, -1), bytearray([0b00001101, 0b11100000, 0b1])),
]


@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remainer = msg.__class__.decode(encoded)
    assert decoded == msg
    assert len(remainer) == 0


@pytest.mark.parametrize("msgcls, args", [
    (SignedFoo, [-3, 512, 1])])
def test_out_of_range(msgcls, args):
    with pytest.raises(ValueError):
        msgcls(*args)
