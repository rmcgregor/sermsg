from __future__ import print_function, absolute_import

from sermsg import Msg, field
from enum import Enum, IntEnum
import pytest


class Colour(IntEnum):
    RED = 0
    BLUE = 1
    GREEN = 2

class Spam(Enum):
    SPAM = "SPAM"
    EGGS = "EGGS"


class Bar(Msg):
    a = field.Enum(Colour, codec=field.Uint8())

class Foo(Msg):
    a = field.Enum(Spam, codec=field.NullString())


tests = [
    (Bar(Colour.BLUE), b'\x01'),
    (Foo(Spam.EGGS), b"EGGS\x00"),
]


@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remained = msg.__class__.decode(encoded)
    assert decoded.a == pytest.approx(msg.a)
    assert len(remained) == 0