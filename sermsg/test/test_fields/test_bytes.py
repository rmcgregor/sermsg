from __future__ import print_function, absolute_import

from sermsg import Msg, field
import pytest


class Bar(Msg):
    s = field.Bytes(len_codec=field.Uint16())


class UnboundedBar(Msg):
    s = field.UnboundedBytes()


class FixedLenBar(Msg):
    s = field.FixedLenBytes(15)


tests = [
    (Bar(b"hello world"), b"\x0b\x00hello world"),
    (UnboundedBar(b"hello world"), b"hello world"),
    (FixedLenBar(b"hello world\x00\x00\x00\x00"), b"hello world\x00\x00\x00\x00"),
]


@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remainder = msg.__class__.decode(encoded)
    assert decoded == msg
    assert len(remainder) == 0


def test_bad_fixed_len():
    with pytest.raises(ValueError):
        FixedLenBar(b"A" * 300)


def test_bad_len_codec():
    with pytest.raises(TypeError):
        class Bar(Msg):
            s = field.String(len_codec=field.String())
