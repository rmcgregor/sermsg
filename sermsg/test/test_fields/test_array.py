from __future__ import print_function, absolute_import

from sermsg import Msg, field
import pytest

class Bar(Msg):
    a = field.Array(field.Uint8(), len_codec=field.Uint8())


class Bar2(Msg):
    a = field.Array(Bar, len_codec=field.Uint16())


class BarUnbounded(Msg):
    a = field.UnboundedArray(field.Uint16())


class BarFixed(Msg):
    a = field.FixedLenArray(field.Int8(), 4)



context = {"msgs": [Bar, Bar2, BarUnbounded, BarFixed]}


tests = [
    (Bar([1, 2, 4, 5]), b"\x04\x01\x02\x04\x05"),
    (Bar2([Bar([3]), Bar([4, 5])]), b"\x02\x00\x01\x03\x02\x04\x05"),
    (BarUnbounded([4, 5, 6, 7, 8, 9, 10, 11]), b"\x04\x00\x05\x00\x06\x00\x07\x00\x08\x00\x09\x00\x0a\x00\x0b\x00"),
    (BarFixed([1, 2, 3, 4]), b"\x01\x02\x03\x04"),
]

@pytest.mark.codec_test(msgs=[Bar, Bar2, BarUnbounded, BarFixed])
@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remainer = msg.__class__.decode(encoded)
    assert decoded == msg
    assert len(remainer) == 0
