from __future__ import print_function, absolute_import

from sermsg import Msg, field
import pytest


class Bar1(Msg):
    a = field.Uint16()


class Bar2(Msg):
    b = field.Uint32()


class Foo(Msg):
    body = field.Switch([Bar1, Bar2])


class FooDct(Msg):
    body = field.Switch({12: Bar1, 56: Bar2})


tests = [
    (Foo(Bar2(4)), b"\x01\x04\x00\x00\x00"),
    (FooDct(Bar1(9)), b"\x0C\x09\x00")
]


@pytest.mark.parametrize("msg,encoded", tests)
def test_encoding_decoding(msg, encoded):
    msg_encoded = msg.encode()
    assert msg_encoded == encoded
    decoded, remained = msg.__class__.decode(encoded)
    print("decoded=  {!r}\n\rexpected= {!r}".format(decoded, msg))
    assert decoded == msg
    assert len(remained) == 0
