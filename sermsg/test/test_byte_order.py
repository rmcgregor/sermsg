from __future__ import absolute_import, print_function

import sys

import pytest

from sermsg import Msg, field

sermsg_byte_order = ">"


def test_module_order():
    class Foo(Msg):
        a = field.Uint16()
    assert Foo.a.byte_order == ">"

def test_class_order():
    class Foo(Msg):
        _byte_order = "<"
        a = field.Uint16()
    assert Foo.a.byte_order == "<"


def test_field_order():
    class Foo(Msg):
        _byte_order = "<"
        a = field.Uint16(byte_order=">")
    assert Foo.a.byte_order == ">"


def test_invalid_msg_byte_order():
    with pytest.raises(ValueError):
        class Foo(Msg):
            _byte_order = "kljklj"
