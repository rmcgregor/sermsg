from __future__ import print_function, absolute_import

from sermsg import Msg, field
import pytest


def test_iter_fields():
    class Foo(Msg):
        a = field.Uint16()
        b = field.Uint32()

    flds = list(Foo)
    assert flds[0] == Foo.a
    assert flds[1] == Foo.b


def test_encoded_len():
    class Bar(Msg):
        a = field.Uint16()
        b = field.FixedLenBytes(10)

    assert Bar(20, b"\x01"* 10).encoded_len() == 12

def test_encode():
    class Foo(Msg):
        byte_order = "<"

        a = field.Uint32()
        b = field.Uint16()

    f = Foo(20, 32)
    encoded = f.encode()
    assert encoded == b"\x14\x00\x00\x00\x20\x00"
