"""The codegeneration environment"""

import os
import jinja2
from jinja2 import nodes
from jinja2.ext import Extension
from jinja2.exceptions import TemplateRuntimeError


def env_filter(wrapped):
    """Decorator to mark codegen environment filters"""
    wrapped._sermsg_codegen_filter = True
    return wrapped


def env_global(wrapped):
    """Decorator for mark all codgen environment globals"""
    wrapped._sermsg_codegen_global = True
    return wrapped


class Environment(object):
    """The codegeneration environment"""
    def __init__(self, filters=None, globals=None, dirs=None):
        self.filters = filters if filters is not None else {}
        self.globals = globals if globals is not None else {}
        self.dirs = dirs if dirs is not None else []

    def add_filters(self, filters):
        """Add filters too environment"""
        self.filters.update(filters)

    def add_filters_from_module(self, module):
        """Add module attributes decorated with evt_filter to the environment filters"""
        env_filters = {}
        for name, attr in module.__dict__.items():
            if hasattr(attr, "_sermsg_codegen_filter"):
                env_filters[name] = attr
        self.add_filters(env_filters)

    def add_globals(self, globs):
        """Add globals too environment"""
        self.globals.update(globs)

    def add_globals_from_module(self, module):
        """Add module attributes decorated with env_global to the environment globals"""
        env_globals = {}
        for name, attr in module.__dict__.items():
            if hasattr(attr, "_sermsg_codegen_global"):
                env_globals[name] = attr
        self.add_globals(env_globals)

    def add_dir(self, directory):
        """Add a search directory to the environment"""
        self.dirs.append(directory)

    def add_dirs(self, directories):
        """Add a list of search directories to the environment"""
        self.dirs += directories

    def _get_env(self):
        """Get the jinja2 Environment"""
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(self.dirs),
            trim_blocks=True,
            lstrip_blocks=True,
            extensions=[RaiseExtension])
        env.filters.update(self.filters)
        env.globals.update(self.globals)
        return env

    def render(self, template, context):
        """Render a single jinja2 template"""
        return self._get_env.get_template(template).render(template, context)

    def render_dir(self, directory, context, template_ext=".template"):
        """Render a directory of jinja2 templates"""
        env = self._get_env()
        renders = {}

        for root, dirs, files in os.walk(directory):
            for f in files:
                if f.endswith(template_ext):
                    rel_path = os.path.relpath(os.path.join(root, f), directory)
                    renders[rel_path.replace(template_ext, "")] = env.get_template(rel_path).render(context)
        return renders


class RaiseExtension(Extension):
    # This is our keyword(s):
    tags = set(['raise'])

    # See also: jinja2.parser.parse_include()
    def parse(self, parser):
        # the first token is the token that started the tag. In our case we
        # only listen to "raise" so this will be a name token with
        # "raise" as value. We get the line number so that we can give
        # that line number to the nodes we insert.
        lineno = next(parser.stream).lineno

        # Extract the message from the template
        message_node = parser.parse_expression()

        return nodes.CallBlock(
            self.call_method('_raise', [message_node], lineno=lineno),
            [], [], [], lineno=lineno
        )

    def _raise(self, msg, caller):
        raise TemplateRuntimeError(msg)
