from . import env_globals, environment
import imp
import os


def _load_module(name, path):
    return imp.load_module(name, *imp.find_module(
        os.path.basename(path), [os.path.dirname(path)]))


def render(codegen_module, context):
    """Use a codegen module Renderer to render some context"""
    if isinstance(codegen_module, str):
        codegen_module = _load_module(os.path.basename(codegen_module), codegen_module)

    # run the renderer on the codegen target module
    return codegen_module.Renderer.run(context)


class Renderer(object):
    """Interface for all code generation modules

    Default Stages:
        - setup_evt : Setup the environment for this render
        - render : Render the context into some output
        - post_render : Perform any post processing on the rendered output
    """
    def render(self, env, context):
        """Convert the convert into the generated target"""
        return {}

    def post_render(self, env, context, renders):
        """Apply any post processing to the rendered targets"""

        # apply the context metadata to the rendered file name as a
        # str.format
        new_renders = {}
        for k, v in renders.items():
            new_renders[k.format(**context["meta"])] = v
        renders = new_renders

        return new_renders

    def setup_env(self, env):
        """Setup the environment before the render"""
        env.add_globals_from_module(env_globals)

    @classmethod
    def static_src(cls):
        """Any static non templated files associated with the target"""
        return {}

    @classmethod
    def run(cls, context):
        """Run through the Renderer stages"""
        renderer = cls()
        env = environment.Environment()
        renderer.setup_env(env)
        renders = renderer.render(env, context)
        renders = renderer.post_render(env, context, renders)
        return renders
