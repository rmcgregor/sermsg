"""Usage: sermsg-codegen <input> <codegen_module> <outdir> [-m] [-s]

Options:
    <input>             Python module containing the generation context (message definitions, enums, metadata).
    <codegen_module>    Codegen implementation target module.
                        Can be an builtin module or the path to a custom one.
    <outdir>            Output directory.
    -m                  Merge the template output with any existing output
                        in the output directory preserving the USER-CODE
                        sections.
    -s                  Copy across any associated static source files.
"""


from __future__ import absolute_import, print_function

import os
import imp
import re
import sys
import shutil
import logging
from docopt import docopt
import enum

from sermsg.msg import MsgMeta, Msg
from .render import render

logger = logging.getLogger("sermsg.codegen")


def merge_files(existing, new):
    """Merge a new implemention with an existing one."""
    pattern = r'<BEGIN USER-CODE: (.*?)>(.*?)<END USER-CODE>'
    format_string = r'<BEGIN USER-CODE: {}>{}<END USER-CODE>'

    matches = re.finditer(pattern, existing, re.DOTALL)
    user_sections = {m.group(1): m.group(2) for m in matches}

    def repl_fn(m):
        ident, content = m.groups()
        new = user_sections.get(ident, content)
        return format_string.format(ident, new)

    return re.sub(pattern, repl_fn, new, flags=re.DOTALL)


def load_module(name, path):
    return imp.load_module(name, *imp.find_module(
        os.path.basename(path), [os.path.dirname(path)]))


def load_codegen_module(path):
    try:
        return load_module("implementation", path)
    except ImportError:
        pass

    builtin_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "implementations", path)
    try:
        return load_module("implementation", builtin_path)
    except ImportError:
        pass

    raise RuntimeError("Codegen module {!r} could not be found".format(path))


def order_msgs(msgs):
    """Order msgs by nested reference depth"""

    def ident(m):
        return "{}.{}".format(m.__module__, m.__name__)

    depths = {ident(m): 0 for m in msgs}

    def find_depth(m, depth=0):
        depths[ident(m)] = max(depths[ident(m)], depth)
        for c in m._child_msgs:
            find_depth(c, depth+1)

    for m in msgs:
        find_depth(m)

    return sorted(msgs, key=lambda x: depths[ident(x)], reverse=True)


def load_context(path):
    context_module = load_module(os.path.basename(path), path)

    msgs = []
    for n, v in context_module.__dict__.items():
        if isinstance(v, MsgMeta) and issubclass(v, Msg) and v is not Msg:
            msgs.append(v)

    enums = []
    for n, v in context_module.__dict__.items():
        if (isinstance(v, enum.EnumMeta) and (issubclass(v, enum.Enum) or issubclass(v, enum.IntEnum))
           and v is not enum.IntEnum and v is not enum.Enum):
                enums.append(v)

    if "sermsg_codegen_metadata" in context_module.__dict__:
        metadata = context_module.sermsg_codegen_metadata
    else:
        metadata = {"namespace": context_module.__name__}

    context = {"msgs": order_msgs(msgs), "enums": enums, "meta": metadata}
    logger.info("Loaded context {} from module {!r}".format(context, path))
    return context


def output_files(renders, outdir, merge=False):
    # make then output dir if it doesn't exist
    if not os.path.exists(outdir):
        print("Creating directory {}".format(outdir))
        os.mkdir(outdir)

    # write the rendered templates
    for name, content in renders.items():
        operation = "Generating"
        outpath = os.path.join(outdir, name)
        if os.path.isfile(outpath):
            if merge:
                operation = "Merging"
                with open(outpath, "r") as f:
                    existing = f.read()
                content = merge_files(existing, content)
            else:
                print("File {} already exists. Use -m to merge.".format(outpath))
                continue

        print("{} {}".format(operation, outpath))
        with open(os.path.join(outdir, name), "w") as f:
            f.write(content)


def copy_static_src(codegen_module, outdir):
    module_path = os.path.dirname(os.path.abspath(codegen_module.__file__))
    static_src = codegen_module.Renderer.static_src()
    for name, relpath in static_src.items():
        outpath = os.path.join(outdir, name)
        print("Writing {}".format(outpath))
        shutil.copyfile(os.path.join(module_path, relpath), outpath)


def main():
    rc = 0

    try:
        args = docopt(__doc__)
        context = load_context(args['<input>'])
        codegen_module = load_codegen_module(args['<codegen_module>'])
        renders = render(codegen_module, context)
        output_files(renders, args['<outdir>'], args['-m'])
        if args['-s']:
            copy_static_src(codegen_module, args['<outdir>'])
    except RuntimeError as e:
        print("Error: {}".format(e.message), file=sys.stderr)
        rc = -1
    sys.exit(rc)


if __name__ == '__main__':
    main()
