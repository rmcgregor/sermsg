from __future__ import absolute_import

from sermsg import field
from jinja2 import environmentfunction

from .environment import env_global


def get_env_codecs(env):
    templates = env.list_templates()
    return [t for t in templates if t.endswith(".codec")]


def get_fld_codec(env, fldcls):
    name = "field/{}.codec".format(fldcls.__name__)
    codecs = get_env_codecs(env)
    if name in codecs:
        return name
    else:
        parent = fldcls.__mro__[1]
        if parent is object:
            raise ValueError("Unable to find codec for {}".format(fldcls))
        return get_fld_codec(env, parent)


@env_global
@environmentfunction
def codec(env, target):
    if isinstance(target, field.Field):
        return get_fld_codec(env, target.__class__)
    elif isinstance(target, str):
        return "{}".format(target)
