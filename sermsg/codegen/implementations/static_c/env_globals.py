"""jinja2 globals used by this target"""

from sermsg import codegen, field


@codegen.env_global
def is_switch_fld(fld):
    if isinstance(fld, field.Switch):
        return True
    return False
