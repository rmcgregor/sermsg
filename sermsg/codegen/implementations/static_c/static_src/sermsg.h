/* Sermsg Error Codes */
typedef enum {
    SERMSG_SUCCESS,
    SERMSG_ERR_BUFF_OVERRUN,
    SERMSG_ERR_OUT_OF_RANGE,
    SERMSG_ERR_BAD_KEY,
    SERMSG_ERR_INVALID_ENUM,
    SERMSG_ERR_LEN_OUT_OF_RANGE,
    SERMSG_ERR_VALUE_TOO_LARGE,
} sermsg_status_t;