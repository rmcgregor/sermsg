#ifndef BYTESTREAM_H
#define BYTESTREAM_H

/*-----------------------------------------------------------------------------
Required header files
-----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>


/*-----------------------------------------------------------------------------
Public Defines
-----------------------------------------------------------------------------*/
#ifndef BS_ASSERT
#define BS_ASSERT(x)
#endif


#define BS_WRITE_BYTES(bs, val, num_bytes, byte_order) \
    do { \
        int i = (byte_order == BS_LE? 0: num_bytes-1); \
        while (i >= 0 && i < num_bytes) \
        { \
            bs->buf[bs->idx++] = (val >> (i * 8)) & 0xff; \
            i += (byte_order == BS_LE? 1: -1); \
        } \
    } while (0);


#define BS_READ_BYTES(bs, val, val_type, num_bytes, byte_order) \
    do { \
        int i = (byte_order == BS_LE? 0: num_bytes-1); \
        *val = 0; \
        while (i >= 0 && i < num_bytes) \
        { \
            *val += ((val_type) bs->buf[bs->idx++]) << (i * 8); \
            i += (byte_order == BS_LE? 1: -1); \
        } \
    } while (0);



/*-----------------------------------------------------------------------------
Public Data Types
-----------------------------------------------------------------------------*/
typedef enum
{
    BS_LE,
    BS_BE,
} bs_byte_order_t;


typedef struct bytestream
{
    uint8_t *buf;
    size_t max_len;
    size_t idx;
    uint8_t bit_idx;
} bytestream_t;


/*-----------------------------------------------------------------------------
Public Functions
-----------------------------------------------------------------------------*/
static inline void bs_attach(bytestream_t *bs, uint8_t *data, size_t len)
{
    BS_ASSERT(bs);
    BS_ASSERT(data);

    bs->max_len = len;
    bs->idx = 0;
    bs->bit_idx = 0;
    bs->buf = data;
}


static inline const uint8_t* bs_stream(bytestream_t *bs)
{
    return &bs->buf[bs->idx];
}


static inline size_t bs_rem_len(const bytestream_t *bs)
{
    BS_ASSERT(bs);

    return bs->max_len - bs->idx;
}


static inline size_t bs_offset(const bytestream_t *bs)
{
    BS_ASSERT(bs)

    return bs->idx;
}


static inline bool bs_read_uint8(bytestream_t *bs, uint8_t *val, bs_byte_order_t byte_order)
{
    if (bs->idx + 1 > bs->max_len)
    {
        return false;
    }

    *val = bs->buf[bs->idx++];
    return true;
}


static inline bool bs_read_int8(bytestream_t *bs, int8_t *val, bs_byte_order_t byte_order)
{
    return bs_read_uint8(bs, (uint8_t *) val, byte_order);
}


static inline bool bs_read_uint16(bytestream_t *bs, uint16_t *val, bs_byte_order_t byte_order)
{
    BS_ASSERT(bs);
    BS_ASSERT(val);

    if (bs->idx + 2 > bs->max_len)
    {
        return false;
    }

    BS_READ_BYTES(bs, val, uint16_t, 2, byte_order);
    return true;
}


static inline bool bs_read_int16(bytestream_t *bs, int16_t *val, bs_byte_order_t byte_order)
{
    return bs_read_uint16(bs, (uint16_t*) val, byte_order);
}


static inline bool bs_read_uint32(bytestream_t *bs, uint32_t *val, bs_byte_order_t byte_order)
{
    BS_ASSERT(bs);
    BS_ASSERT(val);

    if (bs->idx + 4 > bs->max_len)
    {
        return false;
    }

    BS_READ_BYTES(bs, val, uint32_t, 4, byte_order);
    return true;
}


static inline bool bs_read_int32(bytestream_t *bs, int32_t *val, bs_byte_order_t byte_order)
{
    return bs_read_uint32(bs, (uint32_t*) val, byte_order);
}


static inline bool bs_read_uint64(bytestream_t *bs, uint64_t *val, bs_byte_order_t byte_order)
{
    BS_ASSERT(bs);

    if (bs->idx + 8 > bs->max_len)
    {
        return false;
    }

    BS_READ_BYTES(bs, val, uint64_t, 8, byte_order);
    return true;
}


static inline bool bs_read_int64(bytestream_t *bs, int64_t *val, bs_byte_order_t byte_order)
{
    return bs_read_uint64(bs, (uint64_t *) val, byte_order);
}


static inline bool bs_read(bytestream_t *bs, uint8_t *data, size_t len)
{
    BS_ASSERT(bs);
    BS_ASSERT(data);

    if (bs->idx + len > bs->max_len)
    {
        return false;
    }

    memcpy(data, &bs->buf[bs->idx], len);
    bs->idx += len;
    return true;
}


static inline bool bs_write(bytestream_t *bs, const uint8_t *data, size_t len)
{
    BS_ASSERT(bs);

    if (bs->idx + len > bs->max_len)
    {
        return false;
    }

    memcpy(&bs->buf[bs->idx], data, len);
    bs->idx += len;
    return true;
}


static inline bool bs_write_uint8(bytestream_t *bs, uint8_t val, bs_byte_order_t byte_order)
{
    BS_ASSERT(bs);
    (void) byte_order;

    if (bs->idx + 1 > bs->max_len)
    {
        return false;
    }

    bs->buf[bs->idx++] = val;
    return true;
}


static inline bool bs_write_int8(bytestream_t *bs, int8_t val, bs_byte_order_t byte_order)
{
    return bs_write_uint8(bs, (uint8_t) val, byte_order);
}


static inline bool bs_write_uint16(bytestream_t *bs, uint16_t val, bs_byte_order_t byte_order)
{
    BS_ASSERT(bs);

    if (bs->idx + 2 > bs->max_len)
    {
        return false;
    }

    BS_WRITE_BYTES(bs, val, 2, byte_order);
    return true;
}


static inline bool bs_write_int16(bytestream_t *bs, int16_t val, bs_byte_order_t byte_order)
{
    return bs_write_uint16(bs, (uint16_t) val, byte_order);
}



static inline bool bs_write_uint32(bytestream_t *bs, uint32_t val, bs_byte_order_t byte_order)
{
    BS_ASSERT(bs);

    if (bs->idx + 4 > bs->max_len)
    {
        return false;
    }

    BS_WRITE_BYTES(bs, val, 4, byte_order);
    return true;
}


static inline bool bs_write_int32(bytestream_t *bs, int32_t val, bs_byte_order_t byte_order)
{
    return bs_write_uint32(bs, (uint32_t) val, byte_order);
}


static inline bool bs_write_uint64(bytestream_t *bs, uint64_t val, bs_byte_order_t byte_order)
{
    BS_ASSERT(bs);

    if (bs->idx + 8 > bs->max_len)
    {
        return false;
    }

    BS_WRITE_BYTES(bs, val, 8, byte_order);
    return true;
}


static inline bool bs_write_int64(bytestream_t *bs, int64_t val, bs_byte_order_t byte_order)
{
    return bs_write_uint64(bs, (uint64_t) val, byte_order);
}


static inline bool bs_write_float32(bytestream_t *bs, float val, bs_byte_order_t byte_order)
{
    uint32_t intval;
    memcpy(&intval, &val, 4);
    return bs_write_uint32(bs, (uint32_t) val, byte_order);
}


static inline bool bs_write_float64(bytestream_t *bs, double val, bs_byte_order_t byte_order)
{
    uint32_t intval;
    memcpy(&intval, &val, 4);
    return bs_write_uint32(bs, (uint32_t) val, byte_order);
}


static inline uint32_t bs__bit_mask(size_t x)
{
    return (x >= sizeof(uint32_t) * 8) ?
        (uint32_t) -1 : (1U << x) - 1;
}


static inline bool bs_read_bits(bytestream_t *bs, uint_fast8_t bit_len, uint32_t *p_val)
{
    BS_ASSERT(p_val);
    BS_ASSERT(bit_len < sizeof(uint32_t) * 8);

    /* check there is enough bits to read */
    if (bit_len > (bs->max_len - bs->idx) * 8 - bs->bit_idx)
    {
        return false;
    }

    *p_val = 0;

    uint_fast8_t rem_bit_len = bit_len;
    while (rem_bit_len)
    {
        /* find the next number of bits to read */
        uint_fast8_t read_len = (rem_bit_len > 8 - bs->bit_idx ?
                                 8 - bs->bit_idx : rem_bit_len);

        /* extract the segment of the value */
        uint32_t val_seg = ((bs->buf[bs->idx] >> bs->bit_idx)
                         & bs__bit_mask(read_len));
        /* or the segment into the value at its correct position */
        *p_val |= val_seg << (bit_len - rem_bit_len);

        /* remove from the remaining length */
        rem_bit_len -= read_len;

        /* count the current bit and byte indexes */
        bs->bit_idx += read_len;
        bs->idx += bs->bit_idx / 8;
        bs->bit_idx %= 8;
    }

    return true;
}

static inline bool bs_write_bits(bytestream_t *bs, uint_fast8_t bit_len, uint32_t val)
{
    BS_ASSERT(p_val);
    BS_ASSERT(bit_len < sizeof(uint32_t) * 8);

    /* check there is enough bits to write */
    if (bit_len > bs_rem_len(bs) * 8 + bs->bit_idx)
    {
        return false;
    }

    uint_fast8_t rem_bit_len = bit_len;
    while (rem_bit_len)
    {
        /* find the next number of bits to read */
        uint_fast8_t write_len = (rem_bit_len > 8 - bs->bit_idx ?
                                 8 - bs->bit_idx : rem_bit_len);

        /* clear the target bits */
        bs->buf[bs->idx] &= ~(bs__bit_mask(write_len) << bs->bit_idx);

        /* write the bits */
        bs->buf[bs->idx] |= ((val >> (bit_len - rem_bit_len)) & bs__bit_mask(write_len)) << bs->bit_idx;

        /* remove from the remaining length */
        rem_bit_len -= write_len;

        /* count the current bit and byte indexes */
        bs->bit_idx += write_len;
        bs->idx += bs->bit_idx / 8;
        bs->bit_idx %= 8;
    }

    return true;
}


#endif /* BYTESTREAM_H */
