"""Target implementation render entry point"""

from __future__ import absolute_import, print_function

import os
import subprocess

import sermsg.codegen

from . import filters
from . import env_globals


LOCAL_PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_DIR = os.path.join(LOCAL_PATH, "templates")
CODEC_DIR = os.path.join(LOCAL_PATH, "codecs")

FILTER_FUNCS = {n: v for n, v in filters.__dict__.items() if hasattr(v, "__call__")
                and not n.startswith("_")}


def clang_format(filename, contents):
    """Run clang format on some c source"""
    clang = subprocess.Popen(["clang-format", "-assume-filename={}".format(filename)],
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    outs, errs = clang.communicate(contents.encode())
    return outs.decode()


class Renderer(sermsg.codegen.Renderer):
    """Codegen entry point for static_c implementation"""
    def render(self, env, context):
        return env.render_dir(TEMPLATE_DIR, context)

    def post_render(self, env, context, renders):
        formatted_renders = {}
        for f, c in renders.items():
            # attempt to clang format all the output if possible
            try:
                formatted_renders[f] = clang_format(f, c)
            except (subprocess.CalledProcessError, IOError, OSError):
                formatted_renders[f] = c
        return super(Renderer, self).post_render(env, context, formatted_renders)

    def setup_env(self, env):
        super(Renderer, self).setup_env(env)
        env.add_dir(TEMPLATE_DIR)
        env.add_dir(CODEC_DIR)
        env.add_filters(FILTER_FUNCS)
        env.add_globals_from_module(env_globals)

    @classmethod
    def static_src(self):
        return {"bytestream.h": "static_src/bytestream.h", "sermsg.h": "static_src/sermsg.h"}
