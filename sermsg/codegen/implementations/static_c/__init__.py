
# Place the target render entry point at the module level
from .render import Renderer

__all__ = ["Renderer"]
