import ctypes


class bytestream_t(ctypes.Structure):
    _fields_ = [("buf", ctypes.POINTER(ctypes.c_ubyte)),
                ("max_len", ctypes.c_size_t),
                ("idx", ctypes.c_size_t),
                ("bit_idx", ctypes.c_uint8)]

    @classmethod
    def create(cls, length):
        array_type = ctypes.c_ubyte * length
        array = array_type()
        return cls(array, length, 0)

    @classmethod
    def from_stream(cls, stream):
        array_type = ctypes.c_ubyte * len(stream)
        array = array_type(stream)
        return cls(array, len(stream), 0)

    def to_bytes(self):
        m = self.idx
        if self.bit_idx > 0:
            m += 1
        return bytearray([self.buf[i] for i in range(m)])
