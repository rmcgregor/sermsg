from __future__ import absolute_import

import ctypes
import os
import re
import subprocess
from collections import OrderedDict

import pytest

import sermsg.codegen.implementations.static_c as static_c
from sermsg import field
from sermsg.codegen import codegen
from sermsg.codegen.codegen import copy_static_src, output_files

from . import ctypesgen
from .bytestream import bytestream_t


def snake_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def attach_codec(cls, lib, types):
    """Attach encode, decode and from_python methods to a ctypes msg struct"""
    try:
        lib_encode = getattr(lib, cls.__name__[:-2]+"_encode")
        lib_encode.restype = types['sermsg_status_t']
        lib_encode.argtypes = [ctypes.POINTER(bytestream_t), ctypes.POINTER(cls)]

        lib_decode = getattr(lib, cls.__name__[:-2]+"_decode")
        lib_encode.restype = types['sermsg_status_t']
        lib_encode.argtypes = [ctypes.POINTER(bytestream_t), ctypes.POINTER(cls)]

        def encoder(self):
            bs = bytestream_t.create(512)
            print(bs.idx)
            rc = lib_encode(ctypes.byref(bs), ctypes.byref(self))
            print(bs.idx)
            if rc != 0:
                raise RuntimeError(rc)
            return bs.to_bytes()

        def decoder(cls, stream):
            bs = bytestream_t.from_stream(stream)
            inst = cls()
            rc = lib_decode(ctypes.byref(bs), ctypes.byref(inst))
            if rc != 0:
                raise RuntimeError(rc)
            return inst

        def from_python(cls, pmsg):
            """Convert a python Msg instance to a C types struct"""
            inst = cls()
            for fld in pmsg.__class__:
                value = getattr(pmsg, fld.name)

                # switch fields need to have their key filled in separately
                if isinstance(fld, field.Switch):
                    setattr(inst, fld.name + "_key", fld.lookup_key(getattr(pmsg, fld.name)))

                # array fields need to have their length
                if isinstance(fld, (field.Array, field.Bytes, field.String)):
                    setattr(inst, fld.name + "_len", len(getattr(pmsg, fld.name)))

                if isinstance(fld, field.StringField):
                    value = value.encode()

                if isinstance(value, list):
                    value = OrderedDict(inst._fields_)[fld.name](*value)

                if isinstance(fld, (field.Bytes, field.FixedLenBytes)):
                    value = OrderedDict(inst._fields_)[fld.name](*bytearray(value))

                if isinstance(fld, field.SubMsg):
                    value = types[fld.msgcls.__name__].from_python(value)

                if isinstance(fld, field.Switch):
                    setattr(inst, fld.name + "_key", fld.lookup_key(value))
                    union_value = types[value.__class__.__name__].from_python(value)
                    unioncls = OrderedDict(inst._fields_)[fld.name]

                    for name, cfld in unioncls._fields_:
                        if isinstance(union_value, cfld):
                            value = unioncls(**{name: union_value})
                            break

                # copy the field value onto the struct
                setattr(inst, fld.name, value)
            return inst

        cls.encode = encoder
        cls.decode = classmethod(decoder)
        cls.from_python = classmethod(from_python)
    except AttributeError as e:
        pass


def render_compile_load(context, outdir):
    output = codegen.render(static_c, context)
    output_files(output, outdir)
    copy_static_src(static_c, outdir)

    src_files = [os.path.join(outdir, f) for f in output if f.endswith("c")]
    objects = [f.replace(".c", ".o") for f in src_files]
    here = os.path.abspath(os.path.dirname(__file__))
    includes = [here]

    for f, obj in zip(src_files, objects):
        subprocess.check_call(["gcc", "-std=c99", "-fPIC", "-c", f, "-o", obj] + ["-I{}".format(i) for i in includes])

    lib_path = os.path.join(outdir, "shared.so")
    subprocess.check_call(["gcc", "-shared", "-o", lib_path] + objects)
    lib = ctypes.cdll.LoadLibrary(lib_path)

    ctypedefs = {}
    for f in src_files:
        ctypedefs.update(ctypesgen.get_typedefs(f, [outdir]))

    for msg in context["msgs"]:
        ctypedefs[msg.__name__] = ctypedefs["{}_{}_t".format(context["meta"]["namespace"], snake_case(msg.__name__))]

    for cls in ctypedefs.values():
        if issubclass(cls, ctypes.Structure):
            attach_codec(cls, lib, ctypedefs)

    return ctypedefs


class TargetWrapper(object):
    def __init__(self, working_dir):
        self.working_dir = working_dir

    def wrap(self, context):
        return render_compile_load(context, self.working_dir)


@pytest.fixture
def target_wrapper(tmpdir):
    return TargetWrapper(str(tmpdir))
