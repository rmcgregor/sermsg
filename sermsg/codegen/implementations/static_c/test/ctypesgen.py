"""Use the pycparser to autogenerate ctypes wrappers around the generated c code"""

import os
from pycparser import parse_file, c_ast
import ctypes
from enum import IntEnum

FAKE_CLIB = os.path.join(os.path.dirname(__file__), "fake_libc")


class CtypesEnum(IntEnum):
    """A ctypes-compatible IntEnum superclass."""
    @classmethod
    def from_param(cls, obj):
        return int(obj)


class TypedefVisitor(c_ast.NodeVisitor):
    """Converts a C AST to ctypes structs and enums"""
    def __init__(self):
        super(TypedefVisitor, self).__init__()
        self.types = {}
        self._stdtypes = {"uint8_t": ctypes.c_uint8,
                          "uint16_t": ctypes.c_uint16,
                          "uint32_t": ctypes.c_uint32,
                          "uint64_t": ctypes.c_uint64,
                          "int8_t": ctypes.c_uint8,
                          "int16_t": ctypes.c_uint16,
                          "int32_t": ctypes.c_uint32,
                          "int64_t": ctypes.c_uint64,
                          "size_t": ctypes.c_size_t,
                          "char": ctypes.c_char,
                          "unsigned": ctypes.c_uint,
                          "signed": ctypes.c_int}

    def lookup_type(self, name):
        try:
            return self._stdtypes[name]
        except KeyError:
            return self.types[name]

    def add_type(self, name, typ):
        if typ.__name__ == "Anon":
            if issubclass(typ, CtypesEnum):
                typ = CtypesEnum(name, {m.name: m.value for m in typ})
            else:
                typ = type(name, (typ,), {})
        self.types[name] = typ

    def convert_TypeDecl(self, typedecl):
        if isinstance(typedecl.type, c_ast.IdentifierType):
            fldtyp = self.lookup_type(typedecl.type.names[0])
        else:
            fldtyp = self.convert(typedecl.type)
        return fldtyp

    def convert_ArrayDecl(self, arraydecl):
        fldtyp = self.convert_TypeDecl(arraydecl.type) * int(arraydecl.dim.value)
        return fldtyp

    def convert_PtrDecl(self, ptrdecl):
        return ctypes.POINTER(self.convert(ptrdecl.type))

    def convert_Struct(self, struct):
        fields = []
        for decl in struct.decls:
            fldtyp = self.convert(decl.type)
            fields.append((decl.name, fldtyp))

        name = struct.name if struct.name is not None else "Anon"

        # remove the enums can ctypes can't handle them as struct members
        new_fields = []
        for fldname, fld in fields:
            if issubclass(fld, CtypesEnum):
                fld = ctypes.c_int
            new_fields.append((fldname, fld))

        return type(name, (ctypes.Structure,), {"_fields_": new_fields})

    def convert_Enum(self, enum):
        members = {}
        i = 0
        for val in enum.values.enumerators:
            if val.value is not None:
                i = int(val.value.value)
            members[val.name] = i
            i += 1

        name = enum.name if enum.name is not None else "Anon"
        return CtypesEnum(name, members)

    def convert(self, node):
        return getattr(self, "convert_"+type(node).__name__)(node)

    def convert_Union(self, union):
        fields = []
        for decl in union.decls:
            fldtyp = self.convert(decl.type)
            fields.append((decl.name, fldtyp))

        name = union.name if union.name is not None else "Anon"
        return type(name, (ctypes.Union,), {"_fields_": fields})

    def visit_TypeDecl(self, node):
        if isinstance(node.type, c_ast.Struct):
            self.add_type(node.declname, self.convert(node))
        elif isinstance(node.type, c_ast.Enum):
            self.add_type(node.declname, self.convert(node))

        self.generic_visit(node)


def get_typedefs(file_path, include_dirs):
    include_dirs = ["-I{}".format(FAKE_CLIB)] + ["-I{}".format(d) for d in include_dirs]
    ast = parse_file(file_path, use_cpp=True, cpp_args=["-E"] + include_dirs)
    visitor = TypedefVisitor()
    visitor.visit(ast)
    return visitor.types
