from sermsg import Msg, field

import pytest

class Foo(Msg):
    a = field.NullString(max_len=20)

context = {"msgs": [Foo], "meta": {"namespace": "test_nullstring"}}


@pytest.fixture
def cdefns(target_wrapper):
    return target_wrapper.wrap(context)

def test_nullstring_encode(cdefns):
    foo = cdefns["test_nullstring_foo_t"].from_python(Foo("hello world"))
    encoded = foo.encode()
    assert encoded == b"hello world\x00"

def test_max_len(cdefns):
    with pytest.raises(ValueError):
        foo = cdefns["test_nullstring_foo_t"](b"h" * 30)
