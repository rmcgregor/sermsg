from sermsg import Msg, field

sermsg_codegen_metadata = {
    "namespace": "boberty"
}


class Foo(Msg):
    a = field.Uint64()
    b = field.Uint64()


class Bar(Msg):
    foo = field.SubMsg(Foo)
    array = field.Array(field.SubMsg(Foo))


class Bar2(Msg):
    bar = field.SubMsg(Bar)


class Union(Msg):
    body = field.Switch([Foo, Bar, Bar2])


class Bob(Msg):
    _a_len = field.Len("a")
    a = field.Array(field.Uint8())
