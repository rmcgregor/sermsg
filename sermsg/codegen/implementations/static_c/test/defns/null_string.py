from sermsg import Msg, field


class Foo(Msg):
    a = field.NullString(max_len=20)
