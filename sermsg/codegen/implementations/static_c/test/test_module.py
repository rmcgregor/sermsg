import sys
import os
import subprocess
import enum
from sermsg import Msg, field, codegen
import pytest

class Foo(Msg):
    a = field.Uint32()
    s = field.NullString(max_len=20)


class Bar1(Msg):
    x = field.Uint16()

class Bar2(Msg):
    y = field.Uint32()


class AnEnum(enum.IntEnum):
    A = 1
    B = 4

@pytest.mark.parametrize("fld,value", [
    (field.Array(field.Uint16(), len_codec=field.Uint16(max=30)), [1, 2, 3]),
    (field.Bytes(len_codec=field.Uint16(max=20)), b"hello"),
    (field.Enum(AnEnum), AnEnum.A),
    (field.FixedLenArray(field.Uint32(), 6), [1,2,3,4,5,6]),
    (field.NullString(max_len=50), "hello world"),
    (field.SubMsg(Foo), Foo(23, "kjlkj")),
    (field.String(len_codec=field.Uint16(max=15)), "operation"),
    (field.Switch([Bar1, Bar2]), Bar2(98)),
    (field.Bits(10), 567),
    (field.Bits(7), 120),
    (field.Bits(23), 6844),
    (field.FixedLenBytes(30), b"\x01" * 30)
])
def test_c_codecs(fld, value, target_wrapper):

    class TestMsg(Msg):
        a = fld

    msgs = [Foo, Bar1, Bar2, TestMsg]

    context = {"msgs": msgs, "enums": [AnEnum], "meta": {"namespace": "test_{}".format(fld.__class__.__name__.lower())}}

    ctypedefs = target_wrapper.wrap(context)
    tester = TestMsg(value)
    ctester = ctypedefs['TestMsg'].from_python(tester)

    assert tester.encode() == ctester.encode()
