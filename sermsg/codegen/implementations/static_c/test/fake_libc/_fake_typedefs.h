#ifndef _FAKE_TYPEDEFS_H
#define _FAKE_TYPEDEFS_H

typedef int size_t;

/* C99 exact-width integer types */
typedef int int8_t;
typedef int uint8_t;
typedef int int16_t;
typedef int uint16_t;
typedef int int32_t;
typedef int uint32_t;
typedef int int64_t;
typedef int uint64_t;

/* C99 minimum-width integer types */
typedef int int_least8_t;
typedef int uint_least8_t;
typedef int int_least16_t;
typedef int uint_least16_t;
typedef int int_least32_t;
typedef int uint_least32_t;
typedef int int_least64_t;
typedef int uint_least64_t;

/* C99 fastest minimum-width integer types */
typedef int int_fast8_t;
typedef int uint_fast8_t;
typedef int int_fast16_t;
typedef int uint_fast16_t;
typedef int int_fast32_t;
typedef int uint_fast32_t;
typedef int int_fast64_t;
typedef int uint_fast64_t;

/* C99 integer types capable of holding object pointers */
typedef int intptr_t;
typedef int uintptr_t;

/* C99 greatest-width integer types */
typedef int intmax_t;
typedef int uintmax_t;

/* C99 stdbool.h bool type. _Bool is built-in in C99 */
// typedef _Bool bool;

typedef int va_list;


#endif
