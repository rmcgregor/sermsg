"""Jinja2 filters used by this target"""

from sermsg import field
from sermsg.msg import MsgMeta
import re
from enum import EnumMeta

from jinja2 import contextfilter


def snake_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def codec_name(fld):
    return snake_case(fld.__class__.__name__)


def c_ident(val):
    if isinstance(val, field.Field):
        val = type(val).__name__
    elif isinstance(val, type):
        val = val.__name__
    return snake_case(val)


@contextfilter
def cast(context, val, fld):
    return "(({}){})".format(c_type(context, fld), val)


@contextfilter
def c_type(context, val):
    if isinstance(val, field.Enum):
        return c_type(context, val.enum)
    if isinstance(val, field.Field):
        return _field_c_type(val)
    if isinstance(val, MsgMeta):
        return "{}_{}_t".format(context['meta']['namespace'], c_ident(val.__name__))
    if isinstance(val, EnumMeta):
        return "{}_{}_t".format(context['meta']['namespace'], c_ident(val.__name__))

    raise RuntimeError("No c type for {}".format(val))


def _field_c_type(fld):
    if isinstance(fld, field.PrimitiveField):
        return snake_case(type(fld).__name__) + "_t"
    if isinstance(fld, field.StringField):
        return "char"


@contextfilter
def c_declr(context, fld, msg, name=None):
    if name is None:
        name = fld.name

    if isinstance(fld, field.PrimitiveIntegerField):
        return "{} {}".format(snake_case(type(fld).__name__) + "_t", name)

    if isinstance(fld, field.PrimitiveField):
        return "{} {}".format(snake_case(type(fld).__name__), name)

    if isinstance(fld, field.StringField):
        if fld.max_len is None:
            raise ValueError("{} requires a maximum length".format(fld))
        return "char {}[{}]".format(name, fld.max_len)

    if isinstance(fld, field.Array):
        if fld.max_len is None:
            raise ValueError("{} requires a maximum length".format(fld))
        return "{};\n{}[{}]".format(
            c_declr(context, fld.len_codec, msg, fld.name+"_len"),
            c_declr(context, fld.entry, msg, name), max_len_defn(context, fld, msg))

    if isinstance(fld, field.FixedLenArray):
        return "{}[{}]".format(c_declr(context, fld.entry, msg, name), fld.max_len)

    if isinstance(fld, field.BytesField):
        if fld.max_len is None:
            raise ValueError("{} requires a maximum length".format(fld))
        return "uint8_t {}[{}]".format(name, max_len_defn(context, fld, msg))

    if isinstance(fld, field.SubMsg):
        return "{} {}".format(c_type(context, fld.msgcls), name)

    if isinstance(fld, field.Switch):
        s = "{};\n".format(c_declr(context, fld.key_codec, msg, fld.name+"_key"))
        s += "union {"
        s += "\n".join(c_declr(context, field.SubMsg(opt), msg, c_ident(opt))+";" for _, opt in fld.items())
        s += "}} {}".format(c_ident(fld.name))
        return s

    if isinstance(fld, field.Enum):
        return "{} {}".format(c_type(context, fld.enum), name)

    if isinstance(fld, field.Bits):
        return "{} {}: {}".format("int" if fld.signed else "unsigned int", name, fld.width)

    raise NotImplementedError("Unknown C type declaration for {!r}".format(fld))


@contextfilter
def max_len_defn(context, fld, msg):
    return "{}_{}_{}_MAX_LEN".format(context['meta']['namespace'], c_ident(msg), fld.name).upper()


@contextfilter
def switch_key(context, value, fld, msg):
    return "{}_{}_{}_KEY_{}".format(context['meta']['namespace'], c_ident(msg), fld.name, c_ident(value)).upper()


@contextfilter
def enum_name(context, name, enumcls):
    return "{}_{}_{}".format(context['meta']['namespace'], snake_case(enumcls.__name__), name).upper()


def status(name):
    if name.upper() == "SUCCESS":
        status_code = "SERMSG_{}".format(name).upper()
    else:
        status_code = "SERMSG_ERR_{}".format(name).upper()
    return status_code


def reindent(text, depth=0, indent='    '):
    """Simple re-indentation filter for C-like languages."""
    res = []
    comment = False
    comment_start = 0

    for line in text.split('\n'):
        sline = line.strip()

        if not sline:
            res.append('')
            continue

        elif comment:
            res.append(indent * depth + line[comment_start:])

        else:
            pre_unindent = 1 if sline.startswith('}') else 0
            res.append(indent * (depth - pre_unindent) + sline)
            depth += (
                line.count('{') - line.count('}') +
                line.count('(') - line.count(')') +
                line.count('[') - line.count(']'))

        new_comment = (int(comment) + line.count('/*') - line.count('*/')) > 0
        if new_comment and not comment:
            comment_start = line.rindex('/*')
        comment = new_comment

    return '\n'.join(res)
