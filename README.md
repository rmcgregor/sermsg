# sermsg
[![build status](https://gitlab.com/rmcgregor/sermsg/badges/master/build.svg)](https://gitlab.com/rmcgregor/sermsg/commits/master)
[![coverage report](https://gitlab.com/rmcgregor/sermsg/badges/master/coverage.svg?job=test-coverage)](https://rmcgregor.gitlab.io/sermsg/coverage)

Concise explicit structure serialization with code generation support.

## Structure Serialization
The serialization structure is expressed as ```Msg``` subclasses with ```Field``` instance attributes.
The fields define codecs for the named class attributes and allow you to express exactly how the structure is serialized.
All Built-in definitions can be found in the [fields](sermsg/field.py) module.
Custom fields can be expressed by inheriting from the ```Field``` base type.


### Examples
#### Basics
```python
from sermsg import Msg, field

# serializable entries are described as fields on a Msg class
class Foo(Msg):
    a = field.Uint8() # plain uint8
    b = field.FixedLenArray(field.Int32(), 3) # list of 3 int32
    # fields can be given default values and documentation
    c = field.NullString(default="hello world", doc="my string") # c style string

# initialize the Msg with values, assumes the default if not given
foo = Foo(20, [-565, 23, 120])
# access a field value
print(foo.a)  # prints 20
print(foo.c)  # prints "hello world"
# encode into bytes
encoded = foo.encode()
# decode back from bytes
decoded, remaining_bytes = Foo.decode(encoded)
assert foo == decoded
```
#### Byte Order
```python
# byte order can be specified at the module level, the class level or the field level
# valid byte orders are:
#   '>' = Big-endian
#   '<' = Little-endian
# The default byte order is little-endian

from sermsg import Msg, field

# all Msgs in the module shall be big-endian unless overridden
sermsg_byte_order = ">"

class Foo(Msg):
    # all fields in the class shall be little-endian unless overridden
    _byte_order = "<"
    a = field.Uint16() # little-endian
    b = field.Int16(byte_order=">") # big-endian
```

#### Enums
```python
from sermsg import Msg, field
# All types of enums are supported (including Flags/IntFlag from python 3)
from enum import Enum, IntEnum

class Colour(Enum):
    RED = "red"
    BLUE = "blue"
    GREEN = "green"

class Food(IntEnum):
    SPAM = 1000
    EGGS = 1001

class Foo(Msg):
    # codec is used to define how the Enum value is encoded
    colour = field.Enum(Colour, codec=field.NullString())
    food = field.Enum(Food, codec=field.Uint16())
```
#### Switches (Unions)
```python
from sermsg import Msg, field
from enum import IntEnum

# enums can be used for keys
class SwitchKey(IntEnum):
    OPTION1 = 1
    OPTION2 = 2

# options must be unique Msg subclasses
class Option1(Msg):
    a = field.Int8()

class Option2(Msg):
    x = field.Bool()

class Foo(Msg):
    # switches take dicts mapping keys to encoded/decode targets
    # in this case use the SwitchKey enum encoded as a uint8 as the key
    body = field.Switch(
        {
            SwitchKey.OPTION1: Option1,
            SwitchKey.OPTION2: Option2
        },
        key_codec=field.Enum(SwitchKey, codec=field.Uint8()))

```
#### Variable Length Fields
```python
# The variable length fields (arrays, strings, bytes) are encoded as a length followed
# their value. The encoding of the length can be independently specified.
# A Max length can also be given, the default being the max value of the given len codec.

from sermsg import Msg, field

class Foo(Msg):
    array = field.Array(field.NullString(), len_codec=field.Uint8())
    string = field.String(len_codec=field.Uint16(max=100)) # 100 byte string max
    binary = field.Bytes(len_codec=field.Uint32(max=2**15)) # 32kb max

foo = Foo(["a", "b"], "hello", b"\x02\x04")
```
#### Implicit Fields
```python
# some fields can derive there value inplicitly from others

from sermsg import Msg, field

class Foo(Msg):
    # Attributes prepended with _ are removed from __init__  pos args.
    # The encoded byte length over data_type and data fields.
    _enc_length = field.EncodedLen(["_const", "_array_len", "data"], codec=field.Uint16())
    _const = field.Const(field.Uint8(), 0xa5)
    _array_len = field.Len("Array", codec=field.Uint16())
    array = field.FixedLenArray(field.Uint8(), 5)

# _length has been removed from pos args
foo = Foo([1, 2, 3, 7, 8])
# the value is calculated on request
assert foo._enc_length == 8
assert foo._array_len == 5
```
#### Custom Fields
```python
# Say you have a variable length encoded integer with the following rules:
#   if 0 < n < 256: encoded as 1 byte
#   if 256 =< n < 65536: encoded as 3 bytes leading 8 bit zero and a little endian 16 bit value

from sermsg import Msg, field
import struct

class VarUint(field.Field):
    def encode_bytestream(self, stream, value, msg):
        if 0 < value < 256:
            stream.write_struct("<B", value)
        elif value < 65536:
            stream.write_struct("<BH", 0, value)
        else:
            raise ValueError("Outside of range")
        return stream

    def decode_bytestream(self, stream, msgcls, msgdct):
        val = stream.read_struct("<B", stream)[0]
        if val == 0:
            val = stream.read_struct("<H", stream)[0]
        return val, stream

class Foo(Msg):
    a = VarUint()

assert Foo(0x20).encode() == b"\x20" # value < 256 : single byte encoding
assert Foo(0x3456).encode() == b"\x00\x56\x34" # value >= 256 : multi byte encoding
```
## Code Generation
Code generation is achieved via jinja2 templates.

Each generation target is defined as a python module with a definition of the ```Renderer``` class on its interface. 

The ```sermsg-codegen``` command line tool is provided pass message definitions to codegen targets:
```
Usage: sermsg-codegen <input> <codegen_module> <outdir> [-m] [-s]

Options:
    <input>             Python module containing the message definitions.
    <codegen_module>    Codegen implementation python module.
                        Can be an in build module or the path to a custom one.
    <outdir>            Output directory.
    -m                  Merge the template output with any existing output
                        in the output directory preserving the USER-CODE
                        sections.
    -s                  Copy across any associated static source files.
```
### Inbuild Implementations
* [```static_c```](sermsg/codegen/implementations/static_c)
   C99 with no heap allocations.
   Every variable length field must have a max length defined.
   Support for IntEnums.
